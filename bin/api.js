#!/usr/bin/env node

require('babel-core/register')({
    // Don't compile anything in node_modules unless it's riper
    ignore: /^(?=.*node_modules)(?!.*riper).*/
});

var express = require('express');
var helpers = require('riper/bin/helpers');

/**
 * Module dependencies.
 */

var app = require('../api').default(express);
var http = require('http');

/**
 * Get port from environment and store in Express.
 */

var port = helpers.normalizePort(process.env.API_PORT || '3001');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', helpers.onError(port));
server.on('listening', helpers.onListening(server));

