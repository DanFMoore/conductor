import * as types from "../actions/types";
import getReducer from "riper/reducers/repositories";

export default function reduceCategories(state = {}, action) {
    switch(action.type) {
        case types.CATEGORY_GET_PRODUCTS:
            return {
                ...state,
                products: action.products
            };
    }

    return getReducer('categories')(state, action);
}