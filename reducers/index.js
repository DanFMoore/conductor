import { combineReducers } from "redux";
import { reducer as form } from 'redux-form';
import getRepositoryReducer from "riper/reducers/repositories";
import users from "riper/reducers/users";
import common from "riper/reducers/common";
import request from "riper/reducers/request";
import modals from "riper/reducers/modals";
import products from "./products";
import basket from "./basket";
import categories from "./categories";

export default combineReducers({
    currencies: getRepositoryReducer('currencies'),
    orders: getRepositoryReducer('orders'),
    prices: getRepositoryReducer('prices'),
    publishers: getRepositoryReducer('publishers'),
    purchases: getRepositoryReducer('purchases'),
    types: getRepositoryReducer('types'),
    versions: getRepositoryReducer('versions'),
    categories,
    common,
    products,
    users,
    form,
    request,
    modals,
    basket
});