import * as types from "../actions/types";
import { addRelatedRecords } from "riper/reducers/helpers";

function calculateTotal(products) {
    return products.reduce((sum, product) => {
        return sum + (product.price ? Number(product.price.amount) : 0)
    }, 0);
}

export default function reduceBasket(state = {
    products: [],
    userProductIds: [],
    total: 0,
    lastAdded: null,
    save: true
}, action) {
    switch (action.type) {
        case types.BASKET_ADD_PRODUCT:
            return {
                ...state,
                products: [ ...state.products, action.product ],
                lastAdded: action.product
            };

        case types.BASKET_ADD_PRODUCTS:
            return {
                ...state,
                products: [
                    ...state.products,
                    ...action.products
                ]
            };

        case types.BASKET_RELOAD_PRODUCTS:
            return {
                ...state,
                products: action.products
            };

        case types.BASKET_REMOVE_PRODUCT:
            return {
                ...state,
                products: state.products.filter(product => product.id != action.product.id)
            };

        case types.USER_AUTH:
            if (action.loggedInUser) {
                const productIds = state.products.map(product => product.id);
                const basketProductIds = action.loggedInUser.basketProductIds || [];

                // Remove any that are already in the basket
                const userProductIds = basketProductIds.filter(id => productIds.indexOf(id) === -1);

                return {
                    ...state,
                    userProductIds
                };
            }

            break;

        case types.BASKET_CALCULATE_TOTAL:
            return {
                ...state,
                total: calculateTotal(state.products)
            };

        case types.REPOSITORY_GET_RECORDS:
            // Assign the prices to the basket products
            if (action.repository === 'prices') {
                return {
                    ...state,
                    products: addRelatedRecords(state.products, action.records, 'price', 'productId', true)
                };
            }

            break;

        // Empty basket when logging out
        case types.USER_LOGOUT:
        case types.BASKET_CLEAR:
            return {
                ...state,
                products: [],
                userProductIds: [],
                total: 0,
                lastAdded: null
            }
    }

    return state;
}