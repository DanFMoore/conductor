import * as types from "../actions/types";
import getReducer from "riper/reducers/repositories";
import { addRelatedRecords, getForeignIds } from "riper/reducers/helpers";

export default function reduceProducts(state = {
    records: []
}, action) {
    switch(action.type) {
        case types.REPOSITORY_GET_RECORDS: {
            if (action.repository === 'publishers') {
                return {
                    ...state,
                    records: addRelatedRecords(state.records, action.records, 'publisher', 'publisherId')
                };
            } else if (action.repository === 'categories') {
                return {
                    ...state,
                    records: addRelatedRecords(state.records, action.records, 'category', 'categoryId')
                };
            } else if (action.repository === 'prices') {
                return {
                    ...state,
                    records: addRelatedRecords(state.records, action.records, 'price', 'productId', true)
                };
            } else if (action.repository === 'products') {
                return {
                    ...state,
                    records: action.records,
                    publisherIds: getForeignIds(action.records, 'publisherId'),
                    categoryIds: getForeignIds(action.records, 'categoryId')
                };
            }

            break;
        }

        case types.PRODUCT_ASSIGN_CATEGORY: {
            return {
                ...state,
                records: state.records.map(record => {
                    return {
                        ...record,
                        category: action.category
                    }
                })
            };
        }

        case types.BASKET_NOTIFY_PRODUCTS: {
            return {
                ...state,
                records: state.records.map(record => {
                    return {
                        ...record,
                        inBasket: Boolean(action.products.filter(product => product.id == record.id).length)
                    }
                })
            }
        }

        case types.BASKET_ADD_PRODUCT: {
            return {
                ...state,
                records: state.records.map(record => {
                    return {
                        ...record,
                        inBasket: record.id == action.product.id
                    }
                })
            };
        }
    }

    return getReducer('products')(state, action);
}