This website uses the `Express` framework and instead of using the default `Jade` templating language, it uses `Swig`.

##Setup

First create `.env` from the example `.env.example`.

To set up the modules required to run the website:

    npm install
    
If you're on Windows, you will find this doesn't work, because of `note-gyp` which is a module to compile C++ code. This requires all sorts of crap from Python to Visual Studio and then fucking around a lot. Check the following page on GitHub: [Windows users are not happy](https://github.com/nodejs/node-gyp/issues/629) and more specifically [this comment](https://github.com/nodejs/node-gyp/issues/629#issuecomment-153196245).

You may have to install `node-gyp` globally as well, possibly this is normal or might be to do with Microsoft, I don't know:

    sudo npm install -g node-gyp
    
The website also uses Bower to install client-side scripts in a similar method to npm for server-side scripts. To install this and the client-side modules:

    sudo npm install -g bower
    bower install

###Database

The database uses `PostgreSQL` which is better than the default choice of `MySQL`. It is slightly fiddlier to set up than MySQL though. First install:

    sudo apt-get install postgresql
    
On Windows just download and install it which will ask to set up the superuser (called `postgres`) password for you. You can log in to `pgAdmin` with this as well as using it as connection details for the website, or use it to create a user with only what permissions you need.

On Linux it is a bit more fiddly. To run the commands to import the database, you need to create a new postgres user with the same name as your Linux account name. The easiest way to do this is to sudo a command to create a new superuser, which can be used to run the import scripts, administer the database with pgAdmin and use as the login details for the website:

    sudo -u postgres createuser -d -E -P -r -s yourusername

This will prompt you for a password for the account.

You can also create the user and database and assign necessary permissions via SQL commands by logging in to the console:

    sudo -u postgres psql

The other way is to set a password for the `postgres` user:

    sudo -u postgres psql
    \password
    
Which you can then use in pgAdmin to create the correct user (same as Linux account) and assign whatever permissions you need.

On any operating system, once this is done, run the import script:

    node import-db.sql
    
On Windows, you will have to supply the correct connection details as specified in the example `.env`. On Linux, it will use trust authentication to allow access to the user with the same as the OS account, so you can leave this blank.

The database library used in code is `pg-promise`.

##Views

Views are written in `Swig` and are found in the `resource/views` folder.. This supports many of the same features as `Jade` (inheritance, blocks etc) but has the advantage of using HTML syntax instead of a weird indented syntax. For more information, see [http://paularmstrong.github.io/swig/](http://paularmstrong.github.io/swig/).

Some useful bits that are harder to find in the manual:

* `{% autoescape false %}` turns off the HTML escaping, so that if you have some HTML in a variable, it won't convert it to entities. End the section with `{% endautoescape %}`.
* `{% raw %}` turns off all Swig functionality within the section. This means you can literally output `{{` and `}}`, useful for Angular. End with `{% endraw %}`.

##Stylesheets

Stylesheets are written in `SCSS` and a located in `resources/styles`. They are compiled with `node-sass` into `public/styles`. There are three `npm` in `package.json` to do this:

* `npm run sass` - compile the stylesheets with sourcemap information
* `npm sass-watch` - same as above, but keep a watch for changes files
* `npm sass-build` - compile without sourcemap information

The sourcemaps require a tiny bit of faffing in Chrome due to the source files not being in a publicly accessible location, but all you have to do, is when Chrome prompts, drag the folder into the dev tools from the file explorer window and then maybe click some stuff it tells you to. Works for me anyway.

##Running

To start the app with `nodemon` so that it is automatically restarted when files are changed run:
    
    npm start
    
To watch for sass and client-side JavaScript changes and compile them:

    npm run compile
    
To run the in browser, go to [http://localhost:3000/](http://localhost:3000/)

##Build tools

As mentioned above, npm scripts are used, which normally might be done with `Grunt` or `Gulp`. There is no need for these tools - [How to Use npm as a Build Tool](http://blog.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/).

##Tests

The JavaScript tests are located in the `spec` folder and use the Mocha framework, with Chai for assertions and Sinon for spying / stubbing.

Asynchronous testing brings its own strangeness to the table. The easiest way to handle is in each test method (`it('should... ', function() {...}`) or `beforeEach` etc simply add `done` as a parameter and then call it when all the promises / callbacks are run.

Before running the tests, create `.test.env`, which is the same as `.env` but allows for different settings from normal, for example, database details.

To run the tests:

    npm test
    
This runs the command in `package.json` under `scripts.test`.

To run a specific test folder or file:

    NODE_ENV=test mocha spec/helpers/*.js path/to/spec.js --recursive --timeout 5000
    
You may have to install `mocha` globally to do this:
    
    sudo npm install -g mocha

If you are using PhpStorm, you can run the tests within the IDE's own test runner. Go to Edit Configuration > Defaults > Mocha, fill in `/path/to/conductor` into "Working Directory", click the "..." button next to "Environment Variables" and set `NODE_ENV` to be "test", put `/path/to/conductor/node_modules/mocha` in "Mocha package", `spec/helpers/*.js spec --recursive --timeout 10000` into "Extra Mocha options", select "All in directory" and then fill `/path/to/conductor/spec` into "Test directory".

This should now allow all of the tests to be run contextually from a test folder, file or a specific test case that is open in the editor. You may have to create a new configuration based on the default one by going to Edit Configuration > + Sign (Add New Configuration when hovered over) and then select "Mocha" from the drop down menu. You can then select "Mocha" from the "Select Run/Debug Configuration" drop down and press the green "Run" button. You should also be able to run individual test folders / files / tests by right clicking > Run on the folder or file or the specific test case in the spec file.