import React from "react";
import { connect } from "react-redux";
import Product from "./product";
import { getBySlug } from '../../actions/client/categories';

export class Category extends React.Component {
    componentWillMount() {
        if (this.props.client) {
            this.props.getBySlug(this.props.match.params.category);
        }
    }

    componentWillReceiveProps(props) {
        if (props.match.params !== this.props.match.params) {
            this.componentWillMount();
        }
    }

    render() {
        const category = this.props.category;

        if (!category) {
            return null;
        }

        return <main>
            <div className="panel panel-default">
                <div className="panel-heading">
                    { category.name }
                </div>
                <div className="panel-body">
                    <div dangerouslySetInnerHTML={{__html: category.description }} />
                </div>
            </div>

            <ul className="search-results">
                { this.props.products.map(product => <Product product={product} key={product.id} />) }
            </ul>
        </main>;
    }
}

function mapStateToProps(state) {
    return {
        category: state.categories.records[0],
        products: state.products.records,
        client: typeof window !== 'undefined'
    }
}

export default connect(mapStateToProps, { getBySlug })(Category);