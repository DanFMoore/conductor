import React from "react";
import { Link } from 'react-router-dom';
import { price } from "riper/components/helpers";

export default function Product({ product }) {
    return <li className="panel">
        <div className="panel-body">
            <header>
                <img src="https://placekitten.com/70/140"/>

                <hgroup>
                    <h2><Link to={"/products/" + product.slug}>{ product.name }</Link></h2>
                    { product.price ? <span className="price">{price(product.price.amount)}</span> : null }
                    <h3>{ product.publisher ? product.publisher.name : '' }</h3>
                </hgroup>

                <a href="#" className="fa fa-cloud-download" title="Download">
                    <span>Download</span>
                </a>
            </header>

            <section className="information">
                <h3>
                    <i className="fa fa-info-circle" />
                    Information
                </h3>

                <div dangerouslySetInnerHTML={{__html: product.shortDescription }} />
            </section>
        </div>
    </li>;
}