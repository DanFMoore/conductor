import React from "react";

export default class Complete extends React.Component {
    render() {
        return <main className="panel">
            <div className="panel-body">
                <p>Thank you for completing your order. The items you have purchased are now available in your account.</p>
            </div>
        </main>;
    }
}