import React from "react";
import { connect } from "react-redux";
import { Redirect, Link} from 'react-router-dom';
import Login from "riper/components/forms/login";
import Register from "riper/components/forms/register";
import * as types from "../../actions/types";

class LoginRegister extends React.Component {
    render() {
        if (this.props.actionType === types.USER_REGISTER || this.props.actionType === types.USER_AUTH) {
            return <Redirect to="/checkout/details" />;
        }

        return <main className="panel">
            <div className="panel-body container-fluid">
                <div className="row">
                    <div className="col-sm-6">
                        <h3>Returning Customers</h3>
                        <p>Log in to your account</p>
                        <Login />
                    </div>

                    <div className="col-sm-6">
                        <h3>New Customers</h3>
                        <p>Register for a new account</p>
                        <Register />
                    </div>
                </div>

                <div className="form-group">
                    <Link to="/basket" className="btn btn-default pull-left">Back</Link>
                </div>
            </div>
        </main>;
    }
}

function mapStateToProps(state) {
    return state.users;
}

export default connect(mapStateToProps)(LoginRegister);