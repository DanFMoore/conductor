import React from "react";
import FormFields from "riper/components/forms/address";
import CardDetails from "riper/components/forms/cardDetails";
import { Link, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { address as validate } from "riper/validators/user";
import { saveBillingDetails } from "riper/actions/client/users";
import { price } from "riper/components/helpers";
import { Field } from "redux-form";
import renderField from "riper/components/renderField";

export class BillingDetails extends React.Component {
    componentWillMount() {
        this.setState({
            showStaticDetails: Boolean(this.props.initialValues.name)
        });
    }

    showDetailsForm() {
        this.setState({
            showStaticDetails: false
        });
    }

    submit(values) {
        this.props.saveBillingDetails(values).then(() => {
            this.props.history.push('/checkout/complete');
        });
    }

    render() {
        const {handleSubmit, submitting, products, initialValues, total} = this.props;

        if (products.length === 0) {
            return <Redirect to="/basket" />;
        }

        return <main className="panel">
            <form
                onSubmit={handleSubmit(this.submit.bind(this))}
                className="billing-details-form panel-body container-fluid"
            >
                <div className="row">
                    <div className="col-sm-6">
                        <h3>Billing Details</h3>

                        {this.state.showStaticDetails ?
                        <div>
                            <p>Please make sure your billing details are correct:</p>

                            <p>{initialValues.name}</p>
                            <FormFields inactive={true} values={initialValues} />
                            <button className="btn btn-secondary" onClick={this.showDetailsForm.bind(this)} >
                                Edit
                            </button>
                        </div> :
                        <div>
                            <Field name="name" type="text" component={renderField} label="Name" required={true} />
                            <FormFields inactive={false} />
                        </div>}
                        <p className="required-explanation"><span className="required">*</span> Indicates a required field</p>
                    </div>

                    <div className="col-sm-6">
                        <h3>Order Details</h3>

                        <p>These items will be made available in your account immediately after purchase.</p>

                        <table className="table order-details">
                            <tbody>
                            {products.map(product => <tr key={product.id}>
                                <td>{product.name}</td>
                                <td>{product.price ? price(product.price.amount) : null}</td>
                            </tr>)}
                            <tr className="active">
                                <td>Total</td>
                                <td>{price(total)}</td>
                            </tr>
                            </tbody>
                        </table>

                        <h3>Payment Details</h3>
                        <p>Fill in your credit or debit card details below:</p>
                        <img src="/static/images/credit-card-icons.gif" alt="Cards accepted"/>
                        <CardDetails />
                    </div>
                </div>

                <div className="form-group">
                    <Link to="/basket" className="btn btn-default pull-left">Back</Link>

                    <button type="submit" disabled={submitting} className="btn btn-primary pull-right">
                        Pay Now
                    </button>
                </div>
            </form>
        </main>;
    }
}

const form = reduxForm({
    form: 'register',
    validate
})(BillingDetails);

function mapStateToProps(state) {
    return {
        initialValues: state.users.loggedInUser || {},
        products: state.basket.products,
        total: state.basket.total
    };
}

export default connect(mapStateToProps, { saveBillingDetails })(form);