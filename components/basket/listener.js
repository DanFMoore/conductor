import React from "react";
import { connect } from "react-redux";
import { reloadProducts, loadProductsFromUser } from "../../actions/client/basket";

/**
 * Component that listens for changes to the basket and user and updates accordingly
 */
export class Listener extends React.Component {
    componentWillMount() {
        if (this.props.client) {
            this.props.reloadProducts();
        }
    }

    componentWillReceiveProps(props) {
        // When logging in
        if (props.user && !this.props.user) {
            this.props.loadProductsFromUser();
        }
    }

    // Don't render anything as this component is only for listening to prop changes
    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        client: typeof window !== 'undefined',
        user: state.users.loggedInUser,
        products: state.basket.products
    }
}

export default connect(mapStateToProps, { reloadProducts, loadProductsFromUser })(Listener);