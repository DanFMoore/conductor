import React from "react";
import { connect } from "react-redux";
import { price } from "riper/components/helpers";
import { removeProduct } from "../../actions/client/basket";
import { Link } from 'react-router-dom';

export class Basket extends React.Component {
    render() {
        const { products, total, removeProduct, lastAdded } = this.props;

        if (!products || !products.length) {
            return <main className="panel">
                <div className="panel-body">
                    <p>Your basket is empty.</p>

                    <Link to={lastAdded ? "/products/" + lastAdded.slug : '/'} className="btn btn-default">
                        <i className="fa fa-angle-left" /> Continue Shopping
                    </Link>
                </div>
            </main>
        }

        return <main className="panel panel-default">
            <table className="basket table table-hover table-condensed panel-body">
                <thead>
                    <tr>
                        <th style={{width:"70%"}}>Product</th>
                        <th style={{width:"15%"}}>Price</th>
                        <th style={{width:"15%"}} />
                    </tr>
                </thead>
                <tbody>
                    {products.map(product =>
                    <tr key={product.id}>
                        <td data-th="Product">
                            <div className="row">
                                <div className="col-sm-2 hidden-xs">
                                    <img src="http://placehold.it/100x100" alt="..." className="img-responsive"/>
                                </div>
                                <div className="col-sm-10">
                                    <h4 className="nomargin">{product.name}</h4>
                                </div>
                            </div>
                        </td>
                        <td data-th="Price">{ product.price ? price(product.price.amount) : null}</td>
                        <td data-th="" className="actions">
                            <button className="btn btn-danger btn-sm" onClick={() => removeProduct(product)}>
                                <i className="fa fa-trash-o" />
                            </button>
                        </td>
                    </tr>)}
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        <Link to={lastAdded ? "/products/" + lastAdded.slug : '/'} className="btn btn-default">
                            <i className="fa fa-angle-left" /> Continue Shopping
                        </Link>
                    </td>
                    <td className="hidden-xs text-center"><strong>Total {price(total)}</strong></td>
                    <td>
                        <Link to="/checkout/login-register" className="btn btn-primary btn-block">
                            Checkout <i className="fa fa-angle-right"/>
                        </Link>
                    </td>
                </tr>
                </tfoot>
            </table>
        </main>;
    }
}

function mapStateToProps(state) {
    return {
        products: state.basket.products,
        total: state.basket.total,
        lastAdded: state.basket.lastAdded
    }
}

export default connect(mapStateToProps, { removeProduct })(Basket);