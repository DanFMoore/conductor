import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export function Icon(props) {
    const items = props.products.length;

    return <Link to="/basket" className="basket-icon">
        <span className="items">{items} Item{items === 1 ? '' : 's'}</span>
        <i className="fa fa-shopping-cart" />
    </Link>;
}

function mapStateToProps(state) {
    return {
        products: state.basket.products
    }
}

export default connect(mapStateToProps)(Icon);