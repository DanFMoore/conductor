import React from 'react';

export function AboutUs() {
    return <main className="panel">
        <div className="panel-body">
            <p>Conductor was founded in 2017 by an order of monks who believed that there was a gap in the market for this type of thing.</p>

            <p>Vivamus ac libero consequat velit mollis imperdiet quis eget dui. Donec metus diam, condimentum quis enim sed, iaculis malesuada elit. Suspendisse tincidunt ullamcorper nisl sed varius. In dictum mi in orci facilisis consectetur. Vivamus et elit vitae enim gravida vestibulum. Vivamus efficitur ipsum eu ipsum pretium, ut euismod velit efficitur. Ut a libero enim. Nam fringilla pretium erat, sed aliquet metus. Nulla quam felis, maximus eget blandit eu, blandit quis ante. Etiam pretium augue tellus, luctus pretium orci auctor at. Praesent egestas egestas turpis sed condimentum. Aliquam pulvinar enim sed ante fermentum placerat. Aenean quis pretium purus. Integer vulputate luctus diam, imperdiet molestie lacus mattis nec. Duis ipsum mi, pharetra quis ligula nec, lacinia tempor enim.</p>

            <p>Nulla erat purus, hendrerit id egestas eget, pharetra a urna. Curabitur sed enim ac eros tristique suscipit sit amet eget lacus. Nulla facilisi. Cras ornare ullamcorper felis, fermentum posuere nisl luctus at. Curabitur eu placerat orci, a condimentum neque. Cras aliquet nibh nec porttitor volutpat. Cras sodales vehicula lacus a feugiat. Proin dignissim velit iaculis, ultrices erat eu, eleifend metus. Vestibulum consectetur, augue ut auctor pretium, eros velit feugiat leo, at pulvinar lectus leo at tellus. Praesent blandit orci eros, vel feugiat enim convallis ut. Donec non justo mollis, consequat sem mollis, imperdiet nisl. Aliquam et orci sed dui sagittis sollicitudin. Fusce ut justo ipsum. Donec aliquam enim nec mauris tempor dignissim. Etiam quis dictum eros. Cras rhoncus dui neque, id commodo velit malesuada eu.</p>

            <p>Curabitur porta elit ac auctor molestie. Fusce ac dui non tellus consequat laoreet at at nulla. Phasellus eros ante, venenatis a lacinia in, tincidunt nec dui. Etiam at tempor velit. Pellentesque a eleifend est. Maecenas nisl eros, eleifend eu quam eu, rhoncus faucibus lacus. Duis eu risus ac sapien aliquam consequat. Aliquam erat volutpat. Donec faucibus sapien ligula, et interdum dolor laoreet imperdiet. Morbi volutpat nunc in nibh lobortis fringilla. Duis a pulvinar tellus. Integer commodo dui eu tortor mollis venenatis. Donec sit amet molestie sem, venenatis vehicula augue. Vivamus nec ornare elit. Quisque metus ex, tincidunt non ultrices at, congue sed ex. Quisque ultricies nulla eget tempor fermentum.</p>
        </div>
    </main>;
}

export function Terms() {
    return <main className="panel">
        <div className="panel-body">
            <h1>Terms and Conditions</h1>
            <p>Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern [business name]'s relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.</p>
            <p>The term 'Conductor' or 'us' or 'we' refers to the owner of the website whose registered office is [address]. Our company registration number is [company registration number and place of registration]. The term 'you' refers to the user or viewer of our website.</p>
            <p>The use of this website is subject to the following terms of use:</p>
            <ul><li>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
                <li>This website uses cookies to monitor browsing preferences. If you do allow cookies to be used, personal information may be stored by us for use by third parties.</li>
                <li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
                <li>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
                <li>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
                <li>All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.</li>
                <li>Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li>
                <li>From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>
                <li>Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Northern Ireland, Scotland and Wales.</li>
            </ul>
        </div>
    </main>;
}

export function Privacy() {
    return <main className="panel">
        <div className="panel-body">
            <h1>Privacy Policy</h1>
            <p>This privacy policy is for this website and served by Conductor Ltd and governs the privacy of its users who choose to use it. </p>
            <p>The policy sets out the different areas where user privacy is concerned and outlines the obligations &amp; requirements of the users, the website and website owners. Furthermore the way this website processes, stores and protects user data and information will also be detailed within this policy.</p>
            <h2>The Website</h2>
            <p>This website and its owners take a proactive approach to user privacy and ensure the necessary steps are taken to protect the privacy of its users  throughout their visiting experience. This website complies to all UK national laws and requirements for user privacy.</p>
            <h4>Use of Cookies</h4>
            <p>This website uses cookies to better the users experience while visiting the website. Where applicable this website uses a cookie control system allowing the user on their first visit to the website to allow or disallow the use of cookies on their computer / device. This complies with recent legislation requirements for websites to obtain explicit consent from users before leaving behind or reading files such as cookies on a user's computer / device.</p>
            <p>Cookies are small files saved to the user's computers hard drive that track, save and store information about the user's interactions and usage of  the website. This allows the website, through its server to provide the users with a tailored experience within this website.<br />
                Users are advised that if they wish to deny the use and saving of cookies from this website on to their computers hard drive they should take necessary steps within their web browsers security settings to block all cookies from this website and its external serving vendors.</p>
            <p>This website uses tracking software to monitor its visitors to better understand how they use it. This software is provided by Google Analytics which uses cookies to track visitor usage. The software will save a cookie to your computers hard drive in order to track and monitor your engagement and usage of the website, but will not store, save or collect personal information. You can read Google's privacy policy here for further information [<a href="http://www.google.com/privacy.html" target="_blank">http://www.google.com/privacy.html</a>].<br />
            </p>
            <p>Other cookies may be stored to your computers hard drive by external vendors when this website uses referral programs, sponsored links or adverts. Such cookies are used for conversion and referral tracking and typically expire after 30 days, though some may take longer. No personal information is stored, saved or collected.</p>
            <h2>Contact &amp; Communication</h2>
            <p>Users contacting this website and/or its owners do so at their own discretion and provide any such personal details requested at their own risk. Your personal information is kept private and stored securely until a time it is no longer required or has no use, as detailed in the Data Protection Act 1998. Every effort has been made to ensure a safe and secure form to email submission process but advise users using such form to email processes that they do so at their own risk.<br />
            </p>
            <p>This website and its owners use any information submitted to provide you with further information about the products / services they offer or to assist you in answering any questions or queries you may have submitted. This includes using your details to subscribe you to any email newsletter program the website operates but only if this was made clear to you and your express permission was granted when submitting any form to email process. Or whereby you the consumer have previously purchased from or enquired about purchasing from the company a product or service that the email newsletter relates to. This is by no means an entire list of your user rights in regard to receiving email marketing material.
                Your details are not passed on to any third parties. </p>
            <h2>Email Newsletter</h2>
            <p>This website operates an email newsletter program, used to inform subscribers about products and services supplied by this website. Users can subscribe through an online automated process should they wish to do so but do so at their own discretion. Some subscriptions may be manually processed through prior written agreement with the user. </p>
            <p>Subscriptions are taken in compliance with UK Spam Laws detailed in the Privacy and Electronic Communications Regulations 2003. All personal details relating to subscriptions are held securely and in accordance with the Data Protection Act 1998. No personal details are passed on to third parties nor shared with companies / people outside of the company that operates this website. Under the Data Protection Act 1998 you may request a copy of personal information held about you by this website's email newsletter program. A small fee will be payable. If you would like a copy of the information held on you please write to the business address at the bottom of this policy.</p>
            <p>Email marketing campaigns published by this website or its owners may contain tracking facilities within the actual email. Subscriber activity is tracked and stored in a database for future analysis and evaluation. Such tracked activity may include; the opening of emails, forwarding of emails, the clicking of links within the email content, times, dates and frequency of activity [this is by no far a comprehensive list].<br />
                This information is used to refine future email campaigns and supply the user with more relevant content based around their activity.</p>
            <p>In compliance with UK Spam Laws and the Privacy and Electronic Communications Regulations 2003 subscribers are given the opportunity to un-subscribe at any time through an automated system. This process is detailed at the footer of each email campaign. If an automated un-subscription system is unavailable clear instructions on how to un-subscribe will by detailed instead.</p>
            <h2>External Links</h2>
            <p>Although this website only looks to include quality, safe and relevant external links, users are advised adopt a policy of caution before clicking any external web links mentioned throughout this website.</p>
            <p>The owners of this website cannot guarantee or verify the contents of any externally linked website despite their best efforts. Users should therefore note they click on external links at their own risk and this website and its owners cannot be held liable for any damages or implications caused by visiting any external links mentioned.</p>
            <h2>Adverts and Sponsored Links</h2>
            <p>This website may contain sponsored links and adverts. These will typically be served through our advertising partners, to whom may have detailed privacy policies relating directly to the adverts they serve. </p>
            <p>Clicking on any such adverts will send you to the advertisers website through a referral program which may use cookies and will track the number of referrals sent from this website. This may include the use of cookies which may in turn be saved on your computers hard drive. Users should therefore note they click on sponsored external links at their own risk and this website and its owners cannot be held liable for any damages or implications caused by visiting any external links mentioned.</p>
            <h2>Social Media Platforms</h2>
            <p>Communication, engagement and actions taken through external social media platforms that this website and its owners participate on are custom to the terms and conditions as well as the privacy policies held with each social media platform respectively. </p>
            <p>Users are advised to use social media platforms wisely and communicate / engage upon them with due care and caution in regard to their own privacy and personal details. This website nor its owners will ever ask for personal or sensitive information through social media platforms and encourage users wishing to discuss sensitive details to contact them through primary communication channels such as by telephone or email.</p>
            <p>This website may use social sharing buttons which help share web content directly from web pages to the social media platform in question. Users are advised before using such social sharing buttons that they do so at their own discretion and note that the social media platform may track and save your request to share a web page respectively through your social media platform account.</p>
            <h2>Shortened Links in Social Media</h2>
            <p>This website and its owners through their social media platform accounts may share web links to relevant web pages. By default some social media platforms shorten lengthy urls [web addresses] (this is an example: http://bit.ly/zyVUBo). <br />
            </p>
            <p>Users are advised to take caution and good judgement before clicking any shortened urls published on social media platforms by this website and its owners. Despite the best efforts to ensure only genuine urls are published many social media platforms are prone to spam and hacking and therefore this website and its owners cannot be held liable for any damages or implications caused by visiting any shortened links.</p>
        </div>
    </main>;
}