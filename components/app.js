import React from 'react';
import { Route, Link } from 'react-router-dom';
import { connect } from "react-redux";
import { withRouter } from 'react-router';
import Register from 'riper/components/user/register';
import Login from 'riper/components/user/login';
import Logout from 'riper/components/user/logout';
import Category from './category';
import BasketListener from './basket/listener';
import Basket from './basket';
import Product from './product';
import * as Static from './static';
import NotFoundSwitcher from 'riper/components/notFoundSwitcher';
import BasketIcon from './basket/icon';
import Home from './home';
import LoginRegister from './checkout/loginRegister';
import BillingDetails from './checkout/billingDetails';
import Complete from './checkout/complete';

const App = (props) => {
    const accountDropdown = props.loggedInUser ?
        <li className="dropdown pull-right">
            <Link to="/user/profile" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Account <i className="caret"></i>
            </Link>

            <ul className="dropdown-menu">
                <li>
                    <Link to="/user/profile">Profile</Link>
                </li>
                <li>
                    <Link to="/user/logout">Log Out</Link>
                </li>
            </ul>
        </li> :
        <li className="dropdown pull-right">
            <Link to="/user/login" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Account <i className="caret"></i>
            </Link>

            <ul className="dropdown-menu">
                <li>
                    <Link to="/user/login">Login</Link>
                </li>
                <li>
                    <Link to="/user/register">Register</Link>
                </li>
            </ul>
        </li>;

    return <div>
        <div id="header">
            <form id="search" className="form-inline">
                <div className="input-group">
                    <select name="category" className="form-control">
                        <optgroup label="Samples">
                            <option value="samples">All Samples</option>
                            <option value="1">Pianos</option>
                            <option value="2">Drums</option>
                            <option value="3">Voices</option>
                        </optgroup>

                        <optgroup label="Plugins">
                            <option value="plugins">All Plugins</option>
                            <option value="3">Amplifiers</option>
                            <option value="4">Effects</option>
                            <option value="5">Drum Machines</option>
                        </optgroup>
                    </select>

                    <input type="search" placeholder="Search" name="search-text" className="form-control"/>

                    <div className="input-group-btn">
                        <button className="btn">
                            <i className="fa fa-search"/>
                            <span>Search</span>
                        </button>
                    </div>
                </div>

                <BasketIcon />
            </form>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <ul className="nav navbar-nav">
                        <li><Link to="/">Home</Link></li>

                        <li className="dropdown">
                            <Link
                                to="/types/samples"
                                className="dropdown-toggle"
                                data-toggle="dropdown"
                                role="button"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                Samples <i className="caret"/>
                            </Link>

                            <ul className="dropdown-menu">
                                <li>
                                    <Link to="/types/samples">All Samples</Link>
                                </li>
                                <li role="separator" className="divider"/>
                                <li>
                                    <Link to="/categories/digital-synths">Digital Synths</Link>
                                </li>
                                <li>
                                    <Link to="/categories/analog-synths">Analogue Synths</Link>
                                </li>
                                <li>
                                    <Link to="/categories/drum-synths">Drum Synths</Link>
                                </li>
                                <li>
                                    <Link to="/categories/effects">Effects</Link>
                                </li>
                            </ul>
                        </li>
                        { accountDropdown }
                    </ul>
                </div>
            </nav>
        </div>

        <NotFoundSwitcher>
            <Route exact path="/" component={Home} key="0"/>
            <Route path="/user/register" component={Register} key="1"/>
            <Route path="/user/login" component={Login} key="2"/>
            <Route path="/user/logout" component={Logout} key="3"/>
            <Route path="/categories/:category" component={Category} key="4"/>
            <Route path="/products/:product" component={Product} key="5"/>
            <Route path="/basket" component={Basket} key="6"/>

            <Route path="/about-us" component={Static.AboutUs} key="7"/>
            <Route path="/terms-and-conditions" component={Static.Terms} key="8"/>
            <Route path="/privacy-policy" component={Static.Privacy} key="9"/>

            <Route path="/checkout/login-register" component={LoginRegister} key="10"/>
            <Route path="/checkout/details" component={BillingDetails} key="10"/>
            <Route path="/checkout/complete" component={Complete} key="11"/>
         </NotFoundSwitcher>

        <div className="footer">
            &copy; {new Date().getFullYear()} Conductor

            <div className="pull-right">
                <Link to="/about-us">About Us</Link>&nbsp;|&nbsp;
                <Link to="/terms-and-conditions">Terms and Conditions</Link>&nbsp;|&nbsp;
                <Link to="/privacy-policy">Privacy Policy</Link>
            </div>
        </div>

        <BasketListener />
    </div>;
};

function mapStateToProps(state) {
    return {
        loggedInUser: state.users.loggedInUser
    };
}

// Need withRouter otherwise Links don't work
export default withRouter(connect(mapStateToProps)(App));
