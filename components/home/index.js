import React from "react";
import { connect } from "react-redux";
import { getFeatured } from "../../actions/client/products";
import Product from "../category/product";

export class Home extends React.Component {
    componentWillMount() {
        if (this.props.client) {
            this.props.getFeatured();
        }
    }

    render() {
        return <main>
            <ul className="search-results">
                { this.props.products.map(product => <Product product={product} key={product.id} />) }
            </ul>
        </main>;
    }
}

function mapStateToProps(state) {
    return {
        products: state.products.records,
        client: typeof window !== 'undefined'
    }
}

export default connect(mapStateToProps, { getFeatured })(Home);
