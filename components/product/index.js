import React from "react";
import { connect } from "react-redux";
import { getBySlug } from '../../actions/client/products';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router'
import { price } from "riper/components/helpers";
import { addProduct } from "../../actions/client/basket";
import toastr from "toastr";

export class Product extends React.Component {
    componentWillMount() {
        if (this.props.client) {
            this.props.getBySlug(this.props.match.params.product);
        }
    }

    componentWillReceiveProps(props) {
        if (props.match.params !== this.props.match.params) {
            this.componentWillMount();
        }
    }

    purchase(product) {
        this.props.addProduct(product).then(() => {
            toastr.success('Product added to basket');
            this.props.history.push('/basket');
        });
    }

    render() {
        const { product, category, publisher } = this.props;

        if (!product) {
            return null;
        }

        return <main>
            <div className="panel panel-default">
                <div className="panel-body">
                    <header>
                        <img src="https://placekitten.com/70/140"/>

                        <hgroup>
                            <h2>{ product.name }</h2>
                            { product.price ? <span className="price">{price(product.price.amount)}</span> : null }
                            <h3>{ publisher ? publisher.name : '' }</h3>
                            <h3>{ category ?
                                <Link to={"/categories/" + category.slug}>{ category.name }</Link> :
                                '' }</h3>
                        </hgroup>

                        {product.inBasket ?
                        <Link to="/basket" className="btn btn-primary">
                            View in Basket
                        </Link> :
                        <button className="btn btn-primary" onClick={() => this.purchase(product)}>
                            Purchase
                        </button>}

                        {/*<a href="#" className="fa fa-cloud-download" title="Download">
                            <span>Download</span>
                        </a>*/}
                    </header>

                    <section className="information">
                        <h3>
                            <i className="fa fa-info-circle" />
                            Information
                        </h3>

                        <div dangerouslySetInnerHTML={{__html: product.description }} />
                    </section>
                </div>
            </div>
        </main>;
    }
}

function mapStateToProps(state) {
    return {
        product: state.products.records[0],
        category: state.categories.record,
        publisher: state.publishers.record,
        client: typeof window !== 'undefined'
    }
}

export default withRouter(connect(mapStateToProps, { getBySlug, addProduct })(Product));