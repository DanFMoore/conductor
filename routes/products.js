import { getBySlug } from '../actions/server/products';

export default function products(router) {
    router.get('/:product', (req, res, next) => {
        res.locals.store.dispatch(getBySlug(req.params.product)).then(() => {
            next();
        }).catch(next);
    });

    return router;
};
