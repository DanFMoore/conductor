import { getBySlug } from '../actions/server/categories';

export default function categories(router) {
    router.get('/:category', (req, res, next) => {
        res.locals.store.dispatch(getBySlug(req.params.category)).then(() => {
            next();
        }).catch(next);
    });

    return router;
};
