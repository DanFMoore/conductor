import settings from './resources/settings';
import path from 'path';
import logger from 'morgan';
import categories from './routes/categories';
import products from './routes/products';
import reduce from './reducers';
import { renderServer } from 'riper/redux';
import { getFeatured } from './actions/server/products';
import App from './components/app';
import session from 'express-session';
import riperApp from 'riper/app';

/**
 * Create the express app. Wrapped in a function to aid testing
 *
 * @param {Function} express - passed in to aid testing
 * @returns {Object}
 */
export default function(express) {
    const app = express();

    app.use(logger('dev'));

    app.use(session({
        secret: 'dfgsdfgswgwege',
        resave: false,
        saveUninitialized: true,
        cookie: { secure: false }
    }));

    app.set('view cache', false);
    app.use(express.static(path.join(process.cwd(), 'public')));

    riperApp(app, reduce, settings.env);

    // Overrides for react-redux routes
    app.use('/categories', categories(express.Router()));
    app.use('/products', products(express.Router()));

    // Display featured products on home page
    app.get('/', (req, res, next) => {
        res.locals.store.dispatch(getFeatured()).then(() => {
            next();
        });
    });

    // Catch-all route for rendering react components
    app.get('*', renderServer(App));

    return app;
}
