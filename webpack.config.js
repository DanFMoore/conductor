const path = require('path');
const webpack = require('webpack');
const settings = require('./resources/settings');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "style.css"
});

const plugins = [
    extractSass,
    new webpack.DefinePlugin({
        "process.env": {
            "NODE_ENV": JSON.stringify(process.env.NODE_ENV || "development"),
            "DOMAIN": JSON.stringify(process.env.DOMAIN),
            "API_PORT": JSON.stringify(process.env.API_PORT),
            "API_DOMAIN": JSON.stringify(process.env.API_DOMAIN)
        }
    })
];

if (settings.env === 'production') {
    plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true}));
}

module.exports = {
    entry:  [
        './client.js'
    ],
    output: {
        path: path.join(__dirname, 'public', 'static'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                // Don't compile anything in node_modules unless it's riper
                exclude: /^(?=.*node_modules)(?!.*riper).*/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.scss$/,
                loader: extractSass.extract({
                    loader: [{
                        loader: "css-loader",
                        options: {
                            sourceMap: settings.env !== 'production'
                        }
                    }, {
                        loader: "sass-loader",
                        options: {
                            sourceMap: settings.env !== 'production'
                        }
                    }]
                })
            }
        ]
    },
    // These libraries are included in separate script tags and are available as global variables
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        "window": "window"
    },
    plugins,
    devtool: settings.env === 'production' ? null : 'source-map',
    node: {
        fs: "empty"
    }
};