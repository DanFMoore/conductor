import { connect } from "riper/db";
import settings from "./settings";

export default connect(settings.db);