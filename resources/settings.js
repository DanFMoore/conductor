const dotenv = require("dotenv");

const env = process.env;
dotenv.config({path: env.NODE_ENV === 'test' ? '.test.env' : '.env'});

module.exports = {
    "env": env.NODE_ENV,
    "port": env.PORT,
    "apiPort": env.API_PORT,
    "domain": env.DOMAIN,
    "apiDomain": env.API_DOMAIN,
    "jwtSecret": env.JWT_SECRET,
    "db": {
        "host": env.DB_HOST,
        "port": env.DB_PORT,
        "database": env.DB_NAME,
        "user": env.DB_USER,
        "password": env.DB_PASS
    },
    "countries": require('riper/countries.json')
};