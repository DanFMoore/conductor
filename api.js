import settings from './resources/settings';
import logger from 'morgan';
import repositoriesEndpoints from 'riper/endpoints/repositories';
import userEndpoints from 'riper/endpoints/users';
import prices from './endpoints/prices';
import reduce from './reducers';
import getRepositoryActions from 'riper/actions/server/repositories';
import getUserActions from 'riper/actions/server/users';
import getAsyncValidators from 'riper/validators/server/user';
import * as repositories from './repositories';
import api from 'riper/api';

/**
 * Create the express api app. Wrapped in a function to aid testing
 *
 * @param {Function} express - passed in to aid testing
 * @returns {Object}
 */
export default function(express) {
    const app = express();

    app.use(logger('dev'));
    const users = repositories.users;
    api(app, reduce, settings.env, getUserActions(users));

    app.use('/users/', userEndpoints(express.Router(), getUserActions(users), getAsyncValidators(users)));
    app.use('/prices/', prices(express.Router()));
    app.use('/', repositoriesEndpoints(express.Router(), getRepositoryActions(repositories)));

    return app;
}
