import './resources/styles/style.scss';
import App from './components/app';
import reduce from './reducers';
import { renderClient } from 'riper/redux';
import settings from "./resources/settings";

renderClient(App, reduce, settings.apiDomain);
