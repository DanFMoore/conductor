import * as types from "../../actions/types";
import { expect } from "chai";
import reduce from "../../reducers/basket";

describe('basket reducer', () => {
    var oldState, oldStateEncoded, newState;

    describe(types.BASKET_REMOVE_PRODUCT, () => {
        beforeEach(() => {
            oldState = {
                products: [
                    {
                        id: 1,
                    },
                    {
                        id: 2,
                    },
                    {
                        id: 3,
                    },
                    {
                        id: 4,
                    }
                ]
            };

            oldStateEncoded = JSON.stringify(oldState);

            newState = reduce(oldState, {
                type: types.BASKET_REMOVE_PRODUCT,
                product: { id: 2 }
            })
        });

        it('removes the product', () => {
            expect(newState.products.length).to.equal(3);

            expect(newState.products[0].id).to.equal(1);
            expect(newState.products[1].id).to.equal(3);
            expect(newState.products[2].id).to.equal(4);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.USER_AUTH, () => {
        beforeEach(() => {
            oldState = {
                products: [
                    {
                        id: 1,
                    },
                    {
                        id: 2,
                    },
                    {
                        id: 3,
                    },
                    {
                        id: 4
                    }
                ]
            };

            oldStateEncoded = JSON.stringify(oldState);

            newState = reduce(oldState, {
                type: types.USER_AUTH,
                loggedInUser: {
                    basketProductIds: [4, 5, 6]
                }
            });
        });

        it('adds the new product ids, ignoring existing ids', () => {
            expect(newState.userProductIds).to.deep.equal([5, 6]);
        });

        it('adds an empty array if there basketProductIds are missing', () => {
            newState = reduce(oldState, {
                type: types.USER_AUTH,
                loggedInUser: {
                    basketProductIds: null
                }
            });

            expect(newState.userProductIds).to.deep.equal([]);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.BASKET_CALCULATE_TOTAL, () => {
        beforeEach(() => {
            oldState = {
                products: [
                    {
                        id: 1,
                        price: {
                            amount: 100
                        }
                    },
                    {
                        id: 2,
                        price: {
                            amount: '120'
                        }
                    },
                    {
                        id: 3
                    },
                    {
                        id: 4,
                        price: {
                            amount: 200.99
                        }
                    }
                ]
            };

            oldStateEncoded = JSON.stringify(oldState);

            newState = reduce(oldState, {
                type: types.BASKET_CALCULATE_TOTAL
            });
        });

        it('calculates the total amount', () => {
            expect(newState.total).to.equal(420.99);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe('REPOSITORY_GET_RECORDS with prices', () => {
        beforeEach(() => {
            oldState = {
                products: [
                    {
                        id: 1,
                    },
                    {
                        id: 2,
                    },
                    {
                        id: 3,
                    },
                    {
                        id: 4
                    }
                ]
            };

            oldStateEncoded = JSON.stringify(oldState);

            newState = reduce(oldState, {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'prices',
                records: [
                    {
                        productId: 2,
                        amount: 100
                    },
                    {
                        productId: 5,
                        amount: 120
                    },
                    {
                        productId: 1,
                        amount: 50
                    }
                ]
            });
        });

        it('adds the linked prices to the products', () => {
            expect(newState.products[0].price.amount).to.equal(50);
            expect(newState.products[1].price.amount).to.equal(100);
        });

        it('does not add prices to products that have no linked prices', () => {
            expect(newState.products[2].price).to.equal(undefined);
            expect(newState.products[3].price).to.equal(undefined);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });
});