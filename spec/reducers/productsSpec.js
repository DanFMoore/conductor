import * as types from "../../actions/types";
import {expect} from "chai";
import reduce from "../../reducers/products";

describe('products reducer', () => {
    var oldState, oldStateEncoded, newState, action;

    beforeEach(() => {
        oldState = {
            records: [
                {
                    id: 1,
                    publisherId: 1,
                    categoryId: 11
                },
                {
                    id: 2,
                    publisherId: 2,
                    categoryId: 12
                },
                {
                    id: 3,
                    publisherId: 2,
                    categoryId: 12
                },
                {
                    id: 4,
                    publisherId: 4,
                    categoryId: 14
                },
                {
                    id: 5
                }
            ]
        };

        oldStateEncoded = JSON.stringify(oldState);
    });

    describe('REPOSITORY_GET_RECORDS with publishers repository', () => {
        beforeEach(() => {
            action = {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'publishers',
                records: [
                    {
                        id: 1,
                        name: 'Publisher 1'
                    },
                    {
                        id: 2,
                        name: 'Publisher 2'
                    },
                    {
                        id: 3,
                        name: 'Publisher 3'
                    }
                ]
            };

            newState = reduce(oldState, action);
        });

        it('does nothing if not the publishers repository', () => {
            action.repository = 'users';
            newState = reduce(oldState, action);

            expect(JSON.stringify(newState)).to.equal(oldStateEncoded);
        });

        it('adds Publisher 1 to record 1', () => {
            expect(newState.records[0].publisher).to.equal(action.records[0]);
        });

        it('adds Publisher 2 to records 2 and 3', () => {
            expect(newState.records[1].publisher).to.equal(action.records[1]);
            expect(newState.records[2].publisher).to.equal(action.records[1]);
        });

        it('does not add a publisher to records 4 or 5', () => {
            expect(newState.records[3].publisher).to.equal(undefined);
            expect(newState.records[4].publisher).to.equal(undefined);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe('REPOSITORY_GET_RECORDS with category repository', () => {
        beforeEach(() => {
            action = {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'categories',
                records: [
                    {
                        id: 11,
                        name: 'Category 1'
                    },
                    {
                        id: 12,
                        name: 'Category 2'
                    },
                    {
                        id: 13,
                        name: 'Category 3'
                    }
                ]
            };

            newState = reduce(oldState, action);
        });

        it('adds Category 11 to record 1', () => {
            expect(newState.records[0].category).to.equal(action.records[0]);
        });

        it('adds Category 12 to records 2 and 3', () => {
            expect(newState.records[1].category).to.equal(action.records[1]);
            expect(newState.records[2].category).to.equal(action.records[1]);
        });

        it('does not add a category to records 4 or 5', () => {
            expect(newState.records[3].category).to.equal(undefined);
            expect(newState.records[4].category).to.equal(undefined);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe('REPOSITORY_GET_RECORDS with products repository', () => {
        beforeEach(() => {
            action = {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'products',
                records: oldState.records
            };

            oldState = {};
            newState = reduce(oldState, action);
        });

        it('adds the records to newState.records', () => {
            expect(newState.records).to.equal(action.records);
        });

        it('adds the publisherIds to newState.publisherIds', () => {
            expect(newState.publisherIds).to.deep.equal([1, 2, 4]);
        });

        it('adds the categoryUds to newState.categoryIds', () => {
            expect(newState.categoryIds).to.deep.equal([11, 12, 14]);
        });

        it('does not touch the old state', () => {
            expect(oldState).to.deep.equal({});
        });
    });

    describe('REPOSITORY_GET_RECORDS with prices repository', () => {
        beforeEach(() => {
            action = {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'prices',
                records: [
                    {
                        id: 101,
                        productId: 3,
                        amount: 100
                    },
                    {
                        id: 102,
                        productId: 2,
                        amount: 102
                    }
                ]
            };

            newState = reduce(oldState, action);
        });

        it('adds the price to product id 2', () => {
            expect(newState.records[1].price).to.equal(action.records[1]);
        });

        it('adds the price to product id 3', () => {
            expect(newState.records[2].price).to.equal(action.records[0]);
        });

        it('does not add the price to the other products', () => {
            expect(newState.records[0].price).to.equal(undefined);
            expect(newState.records[3].price).to.equal(undefined);
            expect(newState.records[4].price).to.equal(undefined);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.PRODUCT_ASSIGN_CATEGORY, () => {
        beforeEach(() => {
            action = {
                type: types.PRODUCT_ASSIGN_CATEGORY,
                category: { id: 30 }
            };

            newState = reduce(oldState, action);
        });

        it('adds the category to all products', () => {
            expect(newState.records[0].category).to.deep.equal({ id: 30 });
            expect(newState.records[1].category).to.deep.equal({ id: 30 });
            expect(newState.records[2].category).to.deep.equal({ id: 30 });
            expect(newState.records[3].category).to.deep.equal({ id: 30 });
            expect(newState.records[4].category).to.deep.equal({ id: 30 });
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.BASKET_NOTIFY_PRODUCTS, () => {
        beforeEach(() => {
            action = {
                type: types.BASKET_NOTIFY_PRODUCTS,
                products: [
                    { id: 2 },
                    { id: 3 }
                ]
            };

            newState = reduce(oldState, action);
        });

        it('sets which products are in the basket', () => {
            expect(newState.records[1].inBasket).to.equal(true);
            expect(newState.records[2].inBasket).to.equal(true);
        });

        it('sets which products are not in the basket', () => {
            expect(newState.records[0].inBasket).to.equal(false);
            expect(newState.records[3].inBasket).to.equal(false);
            expect(newState.records[4].inBasket).to.equal(false);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.BASKET_ADD_PRODUCT, () => {
        beforeEach(() => {
            action = {
                type: types.BASKET_ADD_PRODUCT,
                product: { id: 2 }
            };

            newState = reduce(oldState, action);
        });

        it('sets the product that was added as being in the basket', () => {
            expect(newState.records[1].inBasket).to.equal(true);
        });

        it('sets the others as not being in the basket', () => {
            expect(newState.records[0].inBasket).to.equal(false);
            expect(newState.records[2].inBasket).to.equal(false);
            expect(newState.records[3].inBasket).to.equal(false);
            expect(newState.records[4].inBasket).to.equal(false);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });
});