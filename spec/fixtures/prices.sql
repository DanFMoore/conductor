CREATE TABLE prices (
    id integer NOT NULL,
    "productId" bigint NOT NULL,
    "currencyId" bigint NOT NULL,
    start timestamp without time zone NOT NULL,
    "end" timestamp without time zone,
    amount numeric(10,2) NOT NULL,
    "previousAmount" numeric(10,2)
);


--
-- TOC entry 190 (class 1259 OID 16412)
-- Name: prices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE prices_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 190
-- Name: prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE prices_id_seq OWNED BY prices.id;

ALTER TABLE prices DISABLE TRIGGER ALL;

TRUNCATE prices RESTART IDENTITY CASCADE;

INSERT INTO prices (id, "productId", "currencyId", start, "end", amount, "previousAmount")
VALUES
/* Future price */
(1, '1', '1', CURRENT_DATE + integer '9', CURRENT_DATE + integer '11', 126, 130),
/* Past price */
(2, '1', '1', CURRENT_DATE - integer '9', CURRENT_DATE - integer '7', 128, 135),
/* Current price */
(3, '1', '1', CURRENT_DATE - integer '9', CURRENT_DATE + integer '7', 130.99, 139),
/* Future price */
(4, '2', '1', CURRENT_DATE + integer '9', NULL, 127, 130),
/* Past price */
(5, '2', '1', CURRENT_DATE - integer '9', CURRENT_DATE - integer '7', 129, 135),
/* Current price */
(6, '2', '1', CURRENT_DATE - integer '9', NULL, 131, 139);