import sinon from "sinon";
import {expect} from "chai";
import { createTestStore } from "riper/redux";
import { getByCategory, getBySlug, getFeatured } from "../../actions/products";
import * as categories from "../../actions/categories";
import * as publishers from "../../actions/publishers";

describe('products actions', () => {
    var store, getRecords, getByProductIds, func;

    beforeEach(() => {
        getRecords = sinon.stub().callsFake(() => dispatch => {
            dispatch({
                type: 'TEST'
            });

            return Promise.resolve();
        });

        getByProductIds = sinon.stub().callsFake(ids => dispatch => {
            dispatch({
                type: 'GET_BY_PRODUCT_IDS',
                ids
            });

            return Promise.resolve();
        });
    });

    describe('getByCategory', () => {
        beforeEach(() => {
            store = createTestStore({
                products: {
                    records: [
                        {id: 50},
                        {id: 51}
                    ]
                }
            });

            func = getByCategory(getRecords, getByProductIds);

            sinon.stub(publishers, 'getByProductPublisherIds').callsFake(getRecords => () => dispatch => {
                dispatch({
                    type: 'PRODUCT_PUBLISHER_IDS',
                    getRecords
                });

                return Promise.resolve();
            });
        });

        afterEach(() => {
            publishers.getByProductPublisherIds.restore();
        });

        it('calls repositories.getRecords', done => {
            store.dispatch(func({ id: 20})).then(() => {
                sinon.assert.calledWith(getRecords, 'products', {
                    categoryId: 20,
                    active: true
                });

                done();
            }).catch(done);
        });

        it('dispatches repositories.getRecords', done => {
            store.dispatch(func({ id: 20 })).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: 'TEST'
                });

                done();
            }).catch(done);
        });

        it('dispatches PRODUCT_ASSIGN_CATEGORY with the category', done => {
            store.dispatch(func({ id: 20 })).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'PRODUCT_ASSIGN_CATEGORY',
                    category: { id: 20 }
                });

                done();
            }).catch(done);
        });

        it('dispatches publishers.getByProductPublisherIds', done => {
            store.dispatch(func({ id: 20 })).then(() => {
                expect(store.getState().actions[2]).to.deep.equal({
                    type: 'PRODUCT_PUBLISHER_IDS',
                    getRecords
                });

                done();
            }).catch(done);
        });

        it('dispatches getByProductIds', done => {
            store.dispatch(func({ id: 20 })).then(() => {
                expect(store.getState().actions[3]).to.deep.equal({
                    type: 'GET_BY_PRODUCT_IDS',
                    ids: [50, 51]
                });

                done();
            }).catch(done);
        });
    });

    describe('getBySlug', () => {
        var getRecord;

        beforeEach(() => {
            store = createTestStore({
                products: {
                    records: [
                        {
                            id: 1,
                            categoryId: 12,
                            publisherId: 14
                        },
                        {
                            id: 2
                        }
                    ]
                },
                basket: {
                    products: [
                        { id: 2 },
                        { id: 3 }
                    ]
                }
            });

            getRecord = sinon.stub().callsFake((repository, id) => dispatch => {
                dispatch({
                    type: 'GET_RECORD',
                    repository,
                    id
                });

                return Promise.resolve();
            });

            func = getBySlug(getRecords, getRecord, getByProductIds);
        });

        it('calls getRecords with the slug', done => {
            store.dispatch(func('my-first-slug')).then(() => {
                sinon.assert.calledWith(getRecords, 'products', {
                    slug: 'my-first-slug',
                    active: true
                });

                done();
            }).catch(done);
        });

        it('dispatches repositories.getRecords', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: 'TEST'
                });

                done();
            }).catch(done);
        });

        it('dispatches notifyBasketProducts', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'BASKET_NOTIFY_PRODUCTS',
                    products: [
                        { id: 2 },
                        { id: 3 }
                    ]
                });

                done();
            }).catch(done);
        });

        it('dispatches getRecord for the category', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[2]).to.deep.equal({
                    type: 'GET_RECORD',
                    repository: 'categories',
                    id: 12
                });

                done();
            }).catch(done);
        });

        it('dispatches getRecord for the publisher', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[3]).to.deep.equal({
                    type: 'GET_RECORD',
                    repository: 'publishers',
                    id: 14
                });

                done();
            }).catch(done);
        });

        it('dispatches getByProductIds', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[4]).to.deep.equal({
                    type: 'GET_BY_PRODUCT_IDS',
                    ids: [1]
                });

                done();
            }).catch(done);
        });

        it('dispatches notFound if there are no products', done => {
            store = createTestStore({
                products: {
                    records: []
                }
            });

            store.dispatch(func()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'NOT_FOUND'
                });

                done();
            }).catch(done);
        });
    });

    describe('getFeatured', () => {
        beforeEach(() => {
            store = createTestStore({
                products: {
                    records: [
                        { id: 50 },
                        { id: 51 }
                    ]
                }
            });

            getRecords = sinon.stub().callsFake((repository, params) => dispatch => {
                dispatch({
                    type: 'GET_RECORDS',
                    repository,
                    params
                });

                return Promise.resolve();
            });

            func = getFeatured(getRecords, getByProductIds);

            sinon.stub(publishers, 'getByProductPublisherIds').callsFake(getRecords => () => dispatch => {
                dispatch({
                    type: 'PRODUCT_PUBLISHER_IDS',
                    getRecords
                });

                return Promise.resolve();
            });

            sinon.stub(categories, 'getByProductCategoryIds').callsFake(getRecords => () => dispatch => {
                dispatch({
                    type: 'PRODUCT_CATEGORY_IDS',
                    getRecords
                });

                return Promise.resolve();
            });
        });

        afterEach(() => {
            publishers.getByProductPublisherIds.restore();
            categories.getByProductCategoryIds.restore();
        });

        it('dispatches repositories.getRecords for the products', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: 'GET_RECORDS',
                    repository: 'products',
                    params: {
                        active: true,
                        featured: true
                    }
                });

                done();
            }).catch(done);
        });

        it('dispatches getByProductCategoryIds', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'PRODUCT_CATEGORY_IDS',
                    getRecords
                });

                done();
            }).catch(done);
        });

        it('dispatches publishers.getByProductPublisherIds', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[2]).to.deep.equal({
                    type: 'PRODUCT_PUBLISHER_IDS',
                    getRecords
                });

                done();
            }).catch(done);
        });

        it('dispatches getByProductIds', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[3]).to.deep.equal({
                    type: 'GET_BY_PRODUCT_IDS',
                    ids: [50, 51]
                });

                done();
            }).catch(done);
        });
    });
});