import {expect} from "chai";
import * as types from "../../../actions/types";
import { getByProductIds } from "../../../actions/server/prices";
import { createTestStore } from "riper/redux";
import { initDb } from "riper/db";

describe('pricesSpec', () => {
    var store;

    before(done => {
        initDb('./spec/fixtures/prices.sql').then(done);
    });

    beforeEach(() => {
        store = createTestStore();
    });

    describe('getByProductId', () => {
        it('dispatches a current date with a end date', done => {
            store.dispatch(getByProductIds([1])).then(() => {
                const action = store.getState().actions[0];

                expect(action.type).to.equal(types.REPOSITORY_GET_RECORDS);
                expect(action.repository).to.equal('prices');

                expect(action.records.length).to.equal(1);
                expect(action.records[0].id).to.equal(3);
                expect(action.records[0].amount).to.equal(130.99);

                done();
            }).catch(done);
        });

        it('dispatches a current date with no end date', done => {
            store.dispatch(getByProductIds([2])).then(() => {
                const action = store.getState().actions[0];

                expect(action.type).to.equal(types.REPOSITORY_GET_RECORDS);
                expect(action.repository).to.equal('prices');

                expect(action.records.length).to.equal(1);
                expect(action.records[0].id).to.equal(6);
                expect(action.records[0].amount).to.equal(131);

                done();
            }).catch(done);
        });

        it('dispatches multiple prices', done => {
            store.dispatch(getByProductIds([1, 2])).then(() => {
                const action = store.getState().actions[0];

                expect(action.type).to.equal(types.REPOSITORY_GET_RECORDS);
                expect(action.repository).to.equal('prices');

                expect(action.records.length).to.equal(2);
                expect(action.records[0].id).to.equal(3);
                expect(action.records[0].amount).to.equal(130.99);
                expect(action.records[1].id).to.equal(6);
                expect(action.records[1].amount).to.equal(131);

                done();
            }).catch(done);
        });
    });
});