import sinon from "sinon";
import {expect} from "chai";
import { createTestStore } from "riper/redux";
import { getByProductCategoryIds, getBySlug } from "../../actions/categories";
import * as products from "../../actions/products";

describe('categories actions', () => {
    var store, getRecords, func;

    beforeEach(() => {
        getRecords = sinon.stub().callsFake(() => dispatch => {
            dispatch({
                type: 'TEST'
            });

            return Promise.resolve();
        });
    });

    describe('getByProductCategoryIds', () => {
        beforeEach(() => {
            store = createTestStore({
                products: {
                    categoryIds: [1, 3]
                }
            });

            func = getByProductCategoryIds(getRecords);
        });

        it('calls repositories.getRecords', done => {
            store.dispatch(func()).then(() => {
                sinon.assert.calledWith(getRecords, 'categories', {
                    id: [1, 3]
                });

                done();
            }).catch(done);
        });

        it('dispatches repositories.getRecords', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: 'TEST'
                });

                done();
            }).catch(done);
        });
    });

    describe('getBySlug', () => {
        beforeEach(() => {
            store = createTestStore({
                categories: {
                    records: [
                        { id: 5 },
                        { id: 6 }
                    ]
                }
            });

            func = getBySlug(getRecords, 'getByProductIds');

            sinon.stub(products, 'getByCategory').callsFake((getRecords, getByProductIds) => category => dispatch => {
                dispatch({
                    type: 'GET_BY_CATEGORY',
                    category,
                    getRecords,
                    getByProductIds
                });

                return Promise.resolve();
            });
        });

        afterEach(() => {
            products.getByCategory.restore();
        });

        it('calls getRecords with the slug', done => {
            store.dispatch(func('my-first-slug')).then(() => {
                sinon.assert.calledWith(getRecords, 'categories', {
                    slug: 'my-first-slug'
                });

                done();
            }).catch(done);
        });

        it('dispatches repositories.getRecords', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: 'TEST'
                });

                done();
            }).catch(done);
        });

        it('dispatches products.getByCategory', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'GET_BY_CATEGORY',
                    category: { id: 5 },
                    getRecords,
                    getByProductIds: 'getByProductIds'
                });

                done();
            }).catch(done);
        });

        it('dispatches notFound if there are no categories', done => {
            store = createTestStore({
                categories: {
                    records: []
                }
            });

            store.dispatch(func()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'NOT_FOUND'
                });

                done();
            }).catch(done);
        });
    });
});