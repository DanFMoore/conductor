import sinon from "sinon";
import {expect} from "chai";
import { createTestStore } from "riper/redux";
import { getByProductPublisherIds } from "../../actions/publishers";

describe('publishers actions', () => {
    var store, getRecords, func;

    beforeEach(() => {
        getRecords = sinon.stub().callsFake(() => dispatch => {
            dispatch({
                type: 'TEST'
            });

            return Promise.resolve();
        });
    });

    describe('getByProductPublisherIds', () => {
        beforeEach(() => {
            store = createTestStore({
                products: {
                    publisherIds: [1, 3]
                }
            });

            func = getByProductPublisherIds(getRecords);
        });

        it('calls repositories.getRecords', done => {
            store.dispatch(func()).then(() => {
                sinon.assert.calledWith(getRecords, 'publishers', {
                    id: [1, 3]
                });

                done();
            }).catch(done);
        });

        it('dispatches repositories.getRecords', done => {
            store.dispatch(func()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: 'TEST'
                });

                done();
            }).catch(done);
        });
    });
});