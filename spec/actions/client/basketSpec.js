import axios from "axios";
import {expect} from "chai";
import * as types from "../../../actions/types";
import * as prices from "../../../actions/client/prices";
import { reloadProducts, loadProductsFromUser } from "../../../actions/client/basket";
import { createTestStore } from "riper/redux";
import sinon from "sinon";

describe('basket client actions', () => {
    var store;

    beforeEach(() => {
        sinon.stub(axios, 'put').returns(Promise.resolve());

        sinon.stub(prices, 'getByProductIds').callsFake(productIds => dispatch => {
            dispatch({
                type: 'GET_BY_PRODUCT_IDS',
                productIds
            });

            return Promise.resolve();
        });
    });

    afterEach(() => {
        prices.getByProductIds.restore();
        axios.put.restore();
    });

    describe('reloadProducts', () => {
        beforeEach(() => {
            sinon.stub(axios, 'get').returns(Promise.resolve({
                data: [
                    {
                        id: 10,
                        name: 'new name 10'
                    },
                    {
                        id: 11,
                        name: 'new name 11'
                    }
                ]
            }));

            store = createTestStore({
                basket: {
                    products: [
                        {
                            id: 10,
                            name: 'old name 10'
                        },
                        {
                            id: 11,
                            name: 'old name 11'
                        }
                    ]
                },
                users: {
                    loggedInUser: {
                        id: 100
                    }
                }
            });
        });

        afterEach(() => {
            axios.get.restore();
        });

        it('calls axios.get', () => {
            store.dispatch(reloadProducts());

            sinon.assert.calledWith(axios.get, '/products', {
                params: {
                    id: [10, 11],
                    active: true
                }
            });
        });

        it('dispatches BASKET_RELOAD_PRODUCTS', done => {
            store.dispatch(reloadProducts()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: types.BASKET_RELOAD_PRODUCTS,
                    products: [
                        {
                            id: 10,
                            name: 'new name 10'
                        },
                        {
                            id: 11,
                            name: 'new name 11'
                        }
                    ]
                });

                done();
            }).catch(done);
        });

        it('gets the prices for the products', done => {
            store.dispatch(reloadProducts()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'GET_BY_PRODUCT_IDS',
                    productIds: [10, 11]
                });

                done();
            }).catch(done);
        });

        it('recalculates the total', done => {
            store.dispatch(reloadProducts()).then(() => {
                expect(store.getState().actions[2]).to.deep.equal({
                    type: types.BASKET_CALCULATE_TOTAL
                });

                done();
            }).catch(done);
        });

        it('saves the product ids', done => {
            store.dispatch(reloadProducts()).then(() => {
                sinon.assert.calledWith(axios.put, '/users/100', {
                    basketProductIds: [10, 11]
                });

                done();
            }).catch(done);
        });

        it('does not save the product ids if there is no user', done => {
            store = createTestStore({
                basket: {
                    products: [
                        { id: 10 },
                        { id: 11 }
                    ]
                },
                users: {
                    loggedInUser: null
                }
            });

            store.dispatch(reloadProducts()).then(() => {
                sinon.assert.notCalled(axios.put);

                done();
            }).catch(done);
        });

        it('does nothing if there are no products', done => {
            store = createTestStore({
                basket: {
                    products: []
                }
            });

            store.dispatch(reloadProducts()).then(() => {
                sinon.assert.notCalled(axios.get);
                sinon.assert.notCalled(axios.put);

                expect(store.getState().actions.length).to.equal(0);

                done();
            }).catch(done);
        });
    });

    describe('loadProductsFromUser', () => {
        beforeEach(() => {
            sinon.stub(axios, 'get').returns(Promise.resolve({
                data: [
                    {
                        id: 10
                    },
                    {
                        id: 11
                    }
                ]
            }));

            store = createTestStore({
                basket: {
                    userProductIds: [10, 11],
                    products: [
                        { id: 20 },
                        { id: 21 }
                    ]
                },
                users: {
                    loggedInUser: {
                        id: 100
                    }
                }
            });
        });

        afterEach(() => {
            axios.get.restore();
        });

        it('calls axios.get', () => {
            store.dispatch(loadProductsFromUser());

            sinon.assert.calledWith(axios.get, '/products', {
                params: {
                    id: [10, 11],
                    active: true
                }
            });
        });

        it('dispatches BASKET_ADD_PRODUCTS', done => {
            store.dispatch(loadProductsFromUser()).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: types.BASKET_ADD_PRODUCTS,
                    products: [
                        {
                            id: 10
                        },
                        {
                            id: 11
                        }
                    ]
                });

                done();
            }).catch(done);
        });

        it('gets the prices for the products', done => {
            store.dispatch(loadProductsFromUser()).then(() => {
                expect(store.getState().actions[1]).to.deep.equal({
                    type: 'GET_BY_PRODUCT_IDS',
                    productIds: [20, 21]
                });

                done();
            }).catch(done);
        });

        it('recalculates the total', done => {
            store.dispatch(loadProductsFromUser()).then(() => {
                expect(store.getState().actions[2]).to.deep.equal({
                    type: types.BASKET_CALCULATE_TOTAL
                });

                done();
            }).catch(done);
        });

        it('saves the product ids', done => {
            store.dispatch(loadProductsFromUser()).then(() => {
                sinon.assert.calledWith(axios.put, '/users/100', {
                    basketProductIds: [20, 21]
                });

                done();
            }).catch(done);
        });

        it('does nothing if there are no products', done => {
            store = createTestStore({
                basket: {
                    userProductIds: []
                }
            });

            store.dispatch(loadProductsFromUser()).then(() => {
                sinon.assert.notCalled(axios.get);
                sinon.assert.notCalled(axios.put);

                expect(store.getState().actions.length).to.equal(0);

                done();
            }).catch(done);
        });
    });
});