--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.7
-- Dumped by pg_dump version 9.5.7

-- Started on 2017-07-26 02:34:18 BST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 16386)
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(255) DEFAULT '0'::character varying NOT NULL,
    "typeId" bigint,
    description text,
    slug character varying(255)
);


--
-- TOC entry 182 (class 1259 OID 16390)
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 182
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- TOC entry 183 (class 1259 OID 16392)
-- Name: currencies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE currencies (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character(3) NOT NULL,
    symbol character(1) NOT NULL
);


--
-- TOC entry 184 (class 1259 OID 16395)
-- Name: currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 184
-- Name: currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE currencies_id_seq OWNED BY currencies.id;


--
-- TOC entry 185 (class 1259 OID 16397)
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE orders (
    id integer NOT NULL,
    "userId" bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    complete boolean DEFAULT false NOT NULL
);


--
-- TOC entry 186 (class 1259 OID 16401)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 186
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- TOC entry 187 (class 1259 OID 16403)
-- Name: platforms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE platforms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 188 (class 1259 OID 16405)
-- Name: platforms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE platforms (
    id integer DEFAULT nextval('platforms_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 189 (class 1259 OID 16409)
-- Name: prices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prices (
    id integer NOT NULL,
    "productId" bigint NOT NULL,
    "currencyId" bigint NOT NULL,
    start timestamp without time zone NOT NULL,
    "end" timestamp without time zone,
    amount numeric(10,2) NOT NULL,
    "previousAmount" numeric(10,2)
);


--
-- TOC entry 190 (class 1259 OID 16412)
-- Name: prices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 190
-- Name: prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE prices_id_seq OWNED BY prices.id;


--
-- TOC entry 191 (class 1259 OID 16414)
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE products (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    "publisherId" bigint NOT NULL,
    "categoryId" bigint,
    "shortDescription" text,
    description text NOT NULL,
    "platformIds" integer[],
    slug character varying(255),
    active boolean,
    featured boolean DEFAULT false NOT NULL
);


--
-- TOC entry 192 (class 1259 OID 16420)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 192
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- TOC entry 193 (class 1259 OID 16422)
-- Name: publishers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE publishers (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    phone character varying(255)
);


--
-- TOC entry 194 (class 1259 OID 16428)
-- Name: publishers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE publishers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 194
-- Name: publishers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE publishers_id_seq OWNED BY publishers.id;


--
-- TOC entry 195 (class 1259 OID 16430)
-- Name: purchases; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE purchases (
    id integer NOT NULL,
    "orderId" bigint DEFAULT 0 NOT NULL,
    "priceId" bigint DEFAULT 0 NOT NULL,
    "productId" bigint DEFAULT 0 NOT NULL
);


--
-- TOC entry 196 (class 1259 OID 16436)
-- Name: purchases_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE purchases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2294 (class 0 OID 0)
-- Dependencies: 196
-- Name: purchases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE purchases_id_seq OWNED BY purchases.id;


--
-- TOC entry 197 (class 1259 OID 16438)
-- Name: types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE types (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    plural character varying(255) NOT NULL
);


--
-- TOC entry 198 (class 1259 OID 16444)
-- Name: types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2295 (class 0 OID 0)
-- Dependencies: 198
-- Name: types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE types_id_seq OWNED BY types.id;


--
-- TOC entry 199 (class 1259 OID 16446)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    "joinDate" timestamp without time zone NOT NULL,
    password character varying(511) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    "basketProductIds" integer[],
    address1 character varying(255),
    address2 character varying(255),
    town character varying(255),
    county character varying(255),
    country character varying(2),
    postcode character varying(255)
);


--
-- TOC entry 200 (class 1259 OID 16453)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2296 (class 0 OID 0)
-- Dependencies: 200
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 201 (class 1259 OID 16455)
-- Name: versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE versions (
    id integer NOT NULL,
    "productId" bigint NOT NULL,
    "versionNumber" character varying(255) NOT NULL,
    date timestamp without time zone NOT NULL,
    filename character varying(255) NOT NULL,
    current boolean DEFAULT true NOT NULL
);


--
-- TOC entry 202 (class 1259 OID 16462)
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2297 (class 0 OID 0)
-- Dependencies: 202
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE versions_id_seq OWNED BY versions.id;


--
-- TOC entry 2084 (class 2604 OID 16464)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- TOC entry 2085 (class 2604 OID 16465)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY currencies ALTER COLUMN id SET DEFAULT nextval('currencies_id_seq'::regclass);


--
-- TOC entry 2087 (class 2604 OID 16466)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- TOC entry 2089 (class 2604 OID 16467)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices ALTER COLUMN id SET DEFAULT nextval('prices_id_seq'::regclass);


--
-- TOC entry 2090 (class 2604 OID 16468)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- TOC entry 2092 (class 2604 OID 16469)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY publishers ALTER COLUMN id SET DEFAULT nextval('publishers_id_seq'::regclass);


--
-- TOC entry 2096 (class 2604 OID 16470)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases ALTER COLUMN id SET DEFAULT nextval('purchases_id_seq'::regclass);


--
-- TOC entry 2097 (class 2604 OID 16471)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY types ALTER COLUMN id SET DEFAULT nextval('types_id_seq'::regclass);


--
-- TOC entry 2099 (class 2604 OID 16472)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2101 (class 2604 OID 16473)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions ALTER COLUMN id SET DEFAULT nextval('versions_id_seq'::regclass);


--
-- TOC entry 2258 (class 0 OID 16386)
-- Dependencies: 181
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: -
--

COPY categories (id, name, "typeId", description, slug) FROM stdin;
2	Digital Synths	2	<p>Some digital synths for you to peruse</p>	digital-synths
4	Effects	2	<p>Some effects for you to peruse</p>	effects
5	Analog Synths	2	<p>Some analog synths for you to peruse</p>	analog-synths
7	Drum Synths	2	<p>Some drum synths for you to peruse</p>	drum-synths
\.


--
-- TOC entry 2298 (class 0 OID 0)
-- Dependencies: 182
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('categories_id_seq', 7, true);


--
-- TOC entry 2260 (class 0 OID 16392)
-- Dependencies: 183
-- Data for Name: currencies; Type: TABLE DATA; Schema: public; Owner: -
--

COPY currencies (id, name, code, symbol) FROM stdin;
2	US Dollar	USD	T
3	Euro	EUR	T
1	Pound Sterling	GBP	£
\.


--
-- TOC entry 2299 (class 0 OID 0)
-- Dependencies: 184
-- Name: currencies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('currencies_id_seq', 3, true);


--
-- TOC entry 2262 (class 0 OID 16397)
-- Dependencies: 185
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: -
--

COPY orders (id, "userId", date, complete) FROM stdin;
1	1	2016-01-03 00:00:00	f
2	2	2015-09-29 00:00:00	t
\.


--
-- TOC entry 2300 (class 0 OID 0)
-- Dependencies: 186
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('orders_id_seq', 2, true);


--
-- TOC entry 2265 (class 0 OID 16405)
-- Dependencies: 188
-- Data for Name: platforms; Type: TABLE DATA; Schema: public; Owner: -
--

COPY platforms (id, name) FROM stdin;
2	OS X
3	Linux
1	Windows
\.


--
-- TOC entry 2301 (class 0 OID 0)
-- Dependencies: 187
-- Name: platforms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('platforms_id_seq', 3, true);


--
-- TOC entry 2266 (class 0 OID 16409)
-- Dependencies: 189
-- Data for Name: prices; Type: TABLE DATA; Schema: public; Owner: -
--

COPY prices (id, "productId", "currencyId", start, "end", amount, "previousAmount") FROM stdin;
3	4	1	2016-01-01 00:00:00	\N	50.00	\N
4	3	1	2016-01-18 02:31:23	\N	100.00	120.00
5	5	1	2016-01-21 01:31:12	\N	75.00	\N
6	6	1	2016-01-21 01:31:33	\N	80.00	\N
7	4	1	2015-01-01 00:00:00	2015-12-31 23:59:59	99.00	\N
8	4	3	2016-01-21 01:03:15	\N	80.00	\N
9	4	2	2016-01-21 00:03:10	\N	95.00	125.00
\.


--
-- TOC entry 2302 (class 0 OID 0)
-- Dependencies: 190
-- Name: prices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('prices_id_seq', 9, true);


--
-- TOC entry 2268 (class 0 OID 16414)
-- Dependencies: 191
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: -
--

COPY products (id, name, "publisherId", "categoryId", "shortDescription", description, "platformIds", slug, active, featured) FROM stdin;
7	Flange	5	4	<p>Applies the flange effect</p>	<p>This effect applies the flange chorus effect to sound going into it.</p>	\N	flange	t	f
4	Basic Synth	1	2	<p>This is not such a good synth, but it works well for playing in function bands.</p>	<p>This basic synth has all the features function bands would expect, but not proper musicians.</p>\r\n\r\n<p>Features include:</p>\r\n<ul>\n<li>Pressing the keys emits a sound frequency, mostly correlating with the key pressed</li>\r\n<li>Casio keyboard style drum beats</li>\n</ul>	\N	basic-synth	t	t
6	Divine Kit	7	7	<p>Divine Kit is an acoustic drum kit made from samples recorded by producer John Haddad.</p> 	<p>Divine Kit is an acoustic drum kit made from samples recorded by producer John Haddad.</p>\n\n<p>This plugin uses multiple outputs, you may have to setup your DAW correctly in order to use it.</p>	\N	divine-kit	t	t
3	Ace Synth	1	5	<p>This is an ace synthesizer that lets you do all the crazy stuff that you'd expect.</p>	<p>This ace synthesizer lets you do all the crazy stuff that you'd expect.</p>\r\n\r\n<p>Features include:</p>\r\n<ul>\r\n<li>Playing music</li>\r\n<li>Fiddling with settings</li>\n</ul>	{1,2,3}	ace-synth	t	f
5	DigiDrum Pro	6	7	<p>DigiDrum Pro is a five part drum and percussion VST plugin suitable for most genres of digital music production.</p> 	<p>DigiDrum Pro is a five part drum and percussion VST plugin suitable for most genres of digital music production.</p>\n\n<p>It features an intuitive user interface packed with loads of sound editing parameters. The sound itself comes from a wave rom containing reproductions designed to resemble classic percussive instruments. These sounds are processed as you wish through the use of sound parameters and velocity controlled expression parameters.</p>\n\n<ul>\n    <li>5 separate DigiDrum parts with 5 user definable audio outputs</li>\n    <li>27 classic drum and percussion waveforms</li>\n    <li>User wavefile import</li>\n    <li>Parts can easily be layered to create even more complex sounds</li>\n    <li>easy sound preview ensures intuitive preset programming</li>\n    <li>A Versatile filter setup capable of many different timbres</li>\n    <li>Velocity modulation of amp, pitch and filter to 'humanize' the feel of the beat</li>\n    <li>Full VST automation of every sound editing controls</li>\n</ul>\n	\N	digidrum-pro	t	f
\.


--
-- TOC entry 2303 (class 0 OID 0)
-- Dependencies: 192
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('products_id_seq', 7, true);


--
-- TOC entry 2270 (class 0 OID 16422)
-- Dependencies: 193
-- Data for Name: publishers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY publishers (id, name, email, phone) FROM stdin;
6	AudioSonic	\N	\N
7	Axel Schneider	\N	\N
10	Creatorum Genius Lab	\N	\N
11	C Hacki DrumBox LM	\N	\N
9	ODO Synths	stimresp@gmail.com	\N
8	GTG Synths	miksybrandt@hotmail.com	\N
5	DCSI	dcsiproductions@gmail.com	\N
4	DSK Music	http://www.dskmusic.com/contact	\N
3	Piano Vintage	contact@pianovintage.fr	\N
1	Synths R Us	contact@synthsrus.com	0774156 456123
2	SynthMart	sales@synthmart.co.uk	078945 123456
\.


--
-- TOC entry 2304 (class 0 OID 0)
-- Dependencies: 194
-- Name: publishers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('publishers_id_seq', 11, true);


--
-- TOC entry 2272 (class 0 OID 16430)
-- Dependencies: 195
-- Data for Name: purchases; Type: TABLE DATA; Schema: public; Owner: -
--

COPY purchases (id, "orderId", "priceId", "productId") FROM stdin;
1	1	3	4
\.


--
-- TOC entry 2305 (class 0 OID 0)
-- Dependencies: 196
-- Name: purchases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchases_id_seq', 1, true);


--
-- TOC entry 2274 (class 0 OID 16438)
-- Dependencies: 197
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: -
--

COPY types (id, name, plural) FROM stdin;
1	Sample	Samples
2	Plugin	Plugins
3	test1	test2
\.


--
-- TOC entry 2306 (class 0 OID 0)
-- Dependencies: 198
-- Name: types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('types_id_seq', 8, true);


--
-- TOC entry 2276 (class 0 OID 16446)
-- Dependencies: 199
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, username, email, name, "joinDate", password, active, "basketProductIds", address1, address2, town, county, country, postcode) FROM stdin;
5	Jeff	jeff@gmail.com	Jeff Roberts	2015-09-27 09:00:00	Tassword3	t	\N	\N	\N	\N	\N	\N	\N
6	Tom	tom@gmail.com	Tom Thumb	2015-09-29 01:00:00	Tassword6	t	\N	\N	\N	\N	\N	\N	\N
4	Dave	dave@gmail.com	Dave Smith	2015-09-28 08:00:00	Tassword4	t	\N	\N	\N	\N	\N	\N	\N
3	Bob	bob@gmail.com	Bob Jones	2015-09-29 15:00:00	Tassword2	t	\N	\N	\N	\N	\N	\N	\N
2	Jon	jon@gmail.com	Jon Parsons	2015-09-27 07:00:00	Tassword1	t	\N	\N	\N	\N	\N	\N	\N
7	Clive	clive@gmail.com	Clive Sykes	2016-01-01 22:00:00	Tassword8	f	\N	\N	\N	\N	\N	\N	\N
10	jose	jose@eu.gov	José Manuel Barroso	2016-01-02 23:00:00	password9	t	\N	\N	\N	\N	\N	\N	\N
1	dan	dan@gmail.com	Dan Moore	2015-09-28 21:00:00	e71Z+HDqraar7+sdvKA7VFA9MElmTHOnM4Ef8glifgE+DfOo0YQdZ8Pj3KgcH29Val+DJl/hvPUb7qhz1ekY32yOUsz8xLSaE8sNhWi+Bh/MLXyWcxErzatJ4M4sqsymBsEWTHeCsHyseftCZEQXRx58x3eKHp8TlHXw8HpyV0KevU0hbeCUmbEttjqiY5xW9PehBnD9+Ny5nw/5lYvE45eP1MzLxCSXI3CiZ5CxkBsG+WMDjaIcCpfC0XEgNElOyb1fl77h+SRDzToVN/nCGtiF0hhmKS8Ni+4vJXCGMKiJal1iOxuhkoW8ldBNXBXBHLhpzUaADYq0/zAlSViKrg==$c350.9UoYlYEyODR/ppvywaTFCEE2LOD9w7gQglFt49TxyCKPb5BBjJX0cajiCFl/AKW52MWghzwm+rCYnd0pcqQDCQ==	t	\N	\N	\N	\N	\N	\N	\N
11	jeff456	thing@thing888.com	Jeff Anderson	2017-05-30 19:09:38	f+mAtwMJVjm3qH9sYwYDKuOJs4poS8nl0qF0+CiicPjhK03P844QRyBqGZu6DZg0wQjTBb1rn0D0/pSsnJtGnyonVikTqSxMMDP+IdcYZDC+MKHgbV1XxS87OglmWlB3ZYHxpNvWdcmPCXh/HlCm2Kwx+GFKRSubvDJPYcaD+5oezVwQfZt79uwSJNiEz4z9CFsY5GLjqg6TQgvM6L4xKFs7EO1MiMBbzVhKTWpOpC+cXxv1kdN3K+OUjpyWqLoaorsTkT/skYbPPkVSvbTL5HrefUY+507BODl+GA5rh3U4WV3AnP6iq9vqefdzuhRmvuQAmbMxrGBHBYDdEUZChw==$c350.T0qDBukjN/8lwFnxsdLtM4SozxlAbjoMMvqEBbfcx6kjKz1PVUkSAXWf1jfaQ1mOjQkDxYfeBqurwHDEimfcxQ==	t	\N	\N	\N	\N	\N	\N	\N
14	asdf	lkjnljk@awe.com	asdf	2017-06-13 05:12:45	jHa+ems9axNQRcO3QvmwKrteYL7M7DHsmWw05NJ4eUo26fI4UQygQAoemIvWMoWnhomMBwWOGYdBJjvOHgB9mHQeQYcTpHT8IN2L47/hHedgl2Zu9YRJ5+VQ18hMBlzPNCkbV3MmsYemisehrt0bfVtnK2t8yVds1lytJRnuM1XNh/aa7Q75bmXdLGw3dXlYchGQwqzp06I1eZQQwSsF8e0uicXmImJlit59aOj2JQ7cZpB9bl2u+Uim/B4UYvUhHUoTQ+NTCWS1gd5af33KqvxZVXgbxMk2GQRf55xtHwagLna3mqnWDpTeOSdkiDXSnbeuwqGiU7rX6oH4m2pSBA==$c350.lzSo96hP1l6AxMCx1ig0PJlz2BbU52P0GdP+rSgK6YGJWo+8Ti2zOeM8NJOHys3DDooUMfWGJ4RuHt9veiG3CA==	t	\N	\N	\N	\N	\N	\N	\N
15	asdfgh	lkjnljghk@awe.com	asdf	2017-06-13 05:13:24	EQ3fWMlUCEIXcKMltCBO8WZcX6g2LBuO2T2jKxMntOcVRZm70/T3ue+w3q2z6YuxPfunCnFQE2V7vQHfezl6cXyPXMmCfA6ItpZ000yQimUfUtA2OSUhT9rEH0iSQWyjwQ3IGbY0rkH8E4iG6vTExhrlE/55AXf5jNGGvG96iWj1escdDXTf/32PNwOc5bPbeLYJyJ2UUTvx0Wlz4s7u55rxg7X6iMDzq6wv4i/l0OjyJiUXN61Vp6L8r69/kWUYLXvibuSe6A9Ijq7sxSJx/LGP5R9UrKmppk/wqot9X1Ym7gJOj7cJBEPoddA7ft+9EE7jIYtFE/Qnsn5cYCawcw==$c350.jc+UWvYDI1xII8w/fH8v36FYH/JpTcUrjpekD69Mb0KJ+mqUlRh0OZz2kyOJSRxj7BIuRoSvvzEfIfyis76w5g==	t	\N	\N	\N	\N	\N	\N	\N
16	asdfghghj	lkjnljghkghj@awe.com	asdfghj	2017-06-13 05:13:58	gHg4+cX2oCQUoPKTyMoOUZWYjgtjpk/PVoXVbGnvoJ9K5dOj6anABWLVr4mEX+JUfeMHp7P6wT9Na+wgxqB8dRzwr+PZBLamxWF/fsZttp4fcaE3VlMkz8dfn3tksOyalDC1mjNePnqsUqI1BQaF+uqIFgVBozkBUDt3zIBr6gtFWacyoQ/gzkiBKyaJ8CKizMMBz6E3kvE7WxEaoZfx8svRr8os6U68TyjGJDKvIRx9pGkPBWo3upVvDzB3jhMJpRqVbCqX3ZhcCn+C1LDNt/XI9IFt3FaU4xURRJI0JgW/SMxUzlKAWsz/5SvTXdHOcvoR3wO/jEUTGTWG69/5CQ==$c350.lpYx/9mqXeJ9P0+Jr63QH+lGd4SbCaqmzP1LKkHKGECy6sqMioyPNk7s3U3E82wVPDUoHSKSAJCum9QIIph5/g==	t	\N	\N	\N	\N	\N	\N	\N
17	asdfghghjgg	lkjnljghkghjhh@awe.com	asdfghj	2017-06-13 05:16:05	KfzHXxVQ3YJH7FiulAGmHbAdJVyuA4yyiGrYj6p6oxcxrud4g6P1vaENTEUqJ0pDisR3dR+E0kGVegq6BdUWLU+LNwZtyVMozTk6JdVjKVqakfalSZKuFNE/2WGgVmQdiRTQofcE0nS12x8lPRW0uKFoZmWFRXOQ2B+pGgWy8fYnwNNC9YVsI66LQAGk2Bp6E/RQgybD8AN/P8aog50OfvqgwDMMSc5slODLt1KSGZrdqvriVPPXw3+Sp43AZXJ9Doo8hGQ6A2R9pC3IL/rNcmFqNpBpuN5b8md/mFa6VOWTGk0AC6b/RWdmF7nMXNiDWqACIpxGi6MIc0kTBzLO3Q==$c350.qLOsdreQosqeAJbXOpsYEtpEKfU1wooQ0rShJ4ZDjf8y9jEraCNvlK4CAutJwoIy221tUQrvBTIwUveDtndTGg==	t	\N	\N	\N	\N	\N	\N	\N
18	asdfghghjggyy	lkjnljghkghjhhhh@awe.com	asdfghjtt	2017-06-13 05:17:22	v+uDiaaKf7s4EoTKHEEwfn9tT71yRnOmkwnnpa+l8mhhKcHKkSj/9hbuULSt8mFVSoZAL9aGNTftABntCzYoxqVtj75vxZiZEpj0H4oFd7ADoa6LO0d98o04ScZqQDBLPoijcZQXSmE2mlDAkFNSaq6T/lVjgKndPcYirOtuIOEXk3erZ/HPMuxBpvhpM/cnM1jDS7idIabpKjX1Ln7JB+0hvt466DP8N81bJ377G8zia9OcSxg6GgZHAKuPcpkGoAy1hAjOFTyutY87hTDRzjm99NluBcjnzm7+1yMiKoYcnEZBwnMCGx06LlUHbVIJCxCCdTeA5hSXhcxRxiXEjw==$c350.hihS0AOBzWyoz57U+PtK4o3pDWFPGPMYfBUwCgOBmRpOTIb2aflVQc7OYhgvCGJGlxXnwl2uxDCye4tuzjZRgQ==	t	\N	\N	\N	\N	\N	\N	\N
19	asdfghghjggyygg	lkjnljghkghjhhhgh@awe.com	asdfghjtt	2017-06-13 05:17:43	utVi4reZILQYryo4cnVzcvjSIGIq/M8lTIBGrsVOQM+lSywKYe9G0GmfgaLMJ2WDwB+u9gvAVB5dOreZFcNQYahg9Ay/TLhQoOv+o9x21FJr17v7zTVvm6HbTDnhC8+WwTvXNYwWbob3Z6rwwRm2IVTUX2wkklCag+U38QBgAA3RcwmrRIRjD2uOMtAx6woPJeGoHKiU+ydzHJAIT4jEhlatr6OuYg6IoPTOXkyFhBhYSRKwhAGdbCMmCEZAYjBR8zX0Fzg1Xq+pEtFnng5zA61iY/g8P7yYmPcLizg07SeMuzLKDJZCD0QG8W1P4pODIacOyHaXp+BXSevvtfdb0w==$c350.0h3mFiSA2bxV2xw9Y97ZDdFcWM87J0JWFRN3UcB90u25amnRZdAPCgDJSVqnk+oqQ5xDp7gm60O70dU04MwCyQ==	t	\N	\N	\N	\N	\N	\N	\N
20	asdfghghjggyyggj	lkjnljghkghjhhhgjh@awe.com	asdfghjtt	2017-06-13 05:20:13	/GeE77vhOt7Iuwqp5kFgnf1pXMLKBd2zLyEWszrts22gjIFkHThvd4PMisTWIO0pgsXsK1nnRgbrlDY617ckMHTfLttJAwqh6R2x2RCt/5TO7wnGgwXrlWJtXdGqMK8pM0h6N4o4Zt9ERslCJeAzH69kMdfW0JII8pyAlb0M4RYrYYxU9SZB/7xI8j8BxvSh/GdJZAq++hta1Nm/mvFJ6bguqtGQocztWl+G2B9nUvvA2l8ct6TguAzpi2xVXAE+C+QSII4L1pSnLjPxEtDt2kSP1nU4mgJ67xgHiSEUr8leCLlDftFlBnF15jgpqjyHLS3UvuoFmzNFzU+R9cr0oQ==$c350.6T8a458oQIYllHEfB0QBpjxBCrsxIMvmdn/A/Qa/R5OBfmQY3183Jox7vbRS/3pGAF2xxVu1py78StP7ot2eCw==	t	\N	\N	\N	\N	\N	\N	\N
21	asdfghghjggyyggjh	lkjnljghkghjhhhgjhh@awe.com	asdfghjtth	2017-06-13 05:22:11	RN84NMMBXmyhSSzzQkuNHv/0YdWSsAm4KYhUVLb8myt3sjRaDgnDLa1S8DK8Pvj9wYlBwThuUB3t0d8754nue2Avfrq81/hC/2VE5zMAD1ayHGfrzs4zIq9JQFyMVCdx4rBZ/pAgXVKjL2KmpQPaSFqFowGT9vV+RF/BWGO1xyNNFVUHMUaHxi3CN+PAYe6N/MRbpLiM6qjRETfgi3rq13tRn/GsQ/X25tez1HKZuMFE7gkXqlkRdyLsq5h9wAET4xOZ/ZbrX5O4RtgMQfYKFsxF/nF+j1pvp4gsn1B5ldHajTfZmaHZoYrMmC0r+ScurF57u5bquvJcc+fo+BBEeg==$c350.tiNmE+vZjnlVLeB2JIZ7EG0TDnhBNcnfmZyJHh/XWSSdxQpoTXl3RuTsvVYMJhSDOZoR+q91DhmweyGRErQI0g==	t	\N	\N	\N	\N	\N	\N	\N
22	asdfghghjggyyggjhj	jlkjnljghkghjhhhgjhh@awe.com	asdfghjtth	2017-06-13 05:25:14	8KtTv5ttsMRqInwRFRm35ECobjMdn80+BNnZmm9kc0XymzgjfFLQb2X/0LxzI1FeB8gT4IWIwbusnCAmTajuDiis9/4UGWb8pcwYWiHNVeWLCWQjEuzvzjj/ur4kckEANgZjQUdcI3TXRj7RMKY770sNllpuxpLP0RhWXxEJacHgYIjd7rWmxz0ULpYb3Wj+BkyxOSGPUbahTQa8867hxAc+eagCwMyFen4Te7I449R9p6UBanFLCKnDnKCm3BJIChRseH73ercbrRGgjvMAUsZclNayoUi0D0nAYN/9Pvc9rJYA/l8UmZ2g/l5OYooLbivnVw/Px3vay48VJbI0hg==$c350.aAvrEza8lJfqWdZ5Ee+Jj/nmAKZtkQ20AN9WkFurcjgtsVY5kRqlFwdHg5NLSCQ//9VymkblFLaDDmG1yef1bg==	t	\N	\N	\N	\N	\N	\N	\N
23	asdfghghjggyhyggjhj	jlkjnljghkghjhhhhgjhh@awe.com	asdfghjtth	2017-06-13 05:26:24	Sn7ffN1L8SMdOyQnmFEDudUgQPOGcjtDGtViEVZs0seAe26WuLcUxcntKLZF9m1cGfEoi3oTCrB5OeLVUgTdvm58eF7qUimB/x/9vzDoBDqAueAHfjdu9anJpEaL9haaqcx45zZjukHVhwMUfk5N/1jI8esD9ITvhfplCcRNl/H1LKzeGnmzLj8qeVw0ADeMQRqKpHq9QuWiskDvTPnz+eBS/9x0ZtGjqR6bct9knXpLrC6+Q+rnrBnwmG9s9fLHcSvOkX7Y9ms2S/KYvqd3nf4zfotww0XZ+2dOexUhgwCLgP9ZokWugy5xpt4aXn51l4Q/7FGjQiM87QwGqA5Abg==$c350.SY7O5y9Lp8P7Nt+RNNbAqfEaEQ1IygUAyPr8EtV7ucOh8zhUfr7uMon0jFoff+3UWTjQz8QvEGcDe+AEjaVe0A==	t	\N	\N	\N	\N	\N	\N	\N
24	lkjn	lkjn@Awe.com	jkjnl	2017-06-15 03:56:12	FIEFCUlUHvOkGkTU2BLBS7rmbvOAgexG2tjhHMY2d54SmnpD9G8im/fNQIvkvzWdzi7p+cvx5xEDYabgbdIEixRe/SaI4hb81IHBLTmn45/U7sbMTtLe9B84WgrK3ngZNoUU0JuxD/EZniwiqnVpnX1s9ae683A7KikC3ioXrY+soM3Nd5G7bJ33EhPeufkmjR1afy8UW8R3iMvaJfDsattAuh56VmTDjnIwiaaBkYHn6IrNnVTjRwBkujALlanr61EAf4OEMo1r8Z6TLjq0rO2ARXxm09rYYSIzAeiXZSC1H5FdHaWaCzI/zuFYlC5iVlMYu4IecJk9HJ8j5NPOYA==$c350.8ukAkxYhpZausGrpmXcbj1iw+jW4/YJHcPh0SyblHXE2210MAsNTyjALb0cMoVOQIdnUtWO6cRBDgpK42vG0pg==	t	\N	\N	\N	\N	\N	\N	\N
13	dan123	dan.moore.email@gmail.com	Dan Moore	2017-06-05 03:13:28	dZr1GrjaFiQYawSjYnJiF87M+ot70NLY3D6rJ6RXPxwwju+m2ICJRS3K4y9JOzxgM4XLZ+zVByzgdPnXzdFmwT9WcwrI+tePC29+YmU3WPLSs3OKqlLTpdDj1W10gCIBHVmvwTHZcfGnEhg0GbrwYFE2xtPZzqnROUcLCrs91p4ZRRB6Qw5Tft8V0WizZS8NezkvN3ows7rg8Vot+rgiZUUxh8rr4Zasg40Tdv+Ib+O/OobpuxabOXza7/5giLk7s9plpKL6h3axb1EJYwjIiSvej7jH3f8S0F6zW/W3W6p8QaKRXe03KJn4cLxFweqnYuoFcBupZg2bQyGGh3775A==$c350.iN0XOwgLGQNQ0vnraLiI71CN6IxkUHI44bwATbu0aIHGHpmM/QBg63VBVcs5on3ZOas1tHPJiVABFsT04vPUHg==	t	{4,5}	\N	\N	\N	\N	\N	\N
26	uuu	uuu@uuu.com	uuu	2017-07-24 01:55:11	SqaVFXtV2PMgF2qLUJV8rcnSe45f3Wj6SNwNFIUhDaNsLT+UdDDOrk3Qnz8WUSa/UqM2RPYAXIQg4NbJPegIGR+O8SZ2KloXXsMQM9viUiZpWvKPBZJb+3hOOwbj/o2Uy3Qu5NgSVcKhAHgmK5tz3VNwr9qhIscLSI7ed1yIhhPwAC1cMu4ajoWw9TzF4xM81Afb2/Ba6+wYpV7unU8Gt3W67g+FVZF24B5hYFdPB7hS7Jj/zE+xwp6ieBlPggdCAFRvMdSTiivYwJ01AKuYX4bxy3XzymmpBnSzczTwG17lPGH+S5fqkMbosk+pPGQpto/xy6XhcLlBL27Kv0zKWQ==$c350.xk+uzcgvP2SapfXNKxxxJOWg2on1B8ZNgrIE/hep3+dmR3WhtWeqXbkYG0w94Ynj+s9+jqGo5GHdrlQip8f8eA==	t	\N	\N	\N	\N	\N	\N	\N
25	ppp	ppp@ppp.com	hhjhj,h	2017-07-24 01:54:42	MLET+I1OCsRiMzVsBkhOh8+O2q5bcIK4WatsFf8hti1jlxKVU7X+8dDX+fgMZgTzYvUVYFz1rg+n2NE9lHLjTdMreg68y74GRW+Yulbt72tNGxkC2Fu3qI+OXoKLEDOl/z6q6+jKTMRAy7roEec3pw+WgqGmMFoyLaE/tEvF6IT5egqcoXzovUrhegnWJjN/9clitX1OvgaOwgUVIgAYKkTeZ8Wo2G6lFRweAotx5c79nPEZTmR878jUN46FrDuPETBg4soW+VYW4NLf9u/OEX3fvz2HoBZmxd8aYl+8p9ZaQ5FXyo5sm9VH3Om3qJZipAqoCCdHceSUD+eSj6FgZw==$c350.mHsswKvImJW2by4W4ZBur31jl0qxNtULbzE0RCMdHsa+zF5SZ6QssjZCaItT4Dq70mBn1lV2KpfPdFoPyIEklg==	t	{4}	jkhfg	fdf	gdrg	drgd	rr	d
\.


--
-- TOC entry 2307 (class 0 OID 0)
-- Dependencies: 200
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 26, true);


--
-- TOC entry 2278 (class 0 OID 16455)
-- Dependencies: 201
-- Data for Name: versions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY versions (id, "productId", "versionNumber", date, filename, current) FROM stdin;
1	5	1.2	2016-01-21 00:05:26	File-1.2.zip	t
2	5	1.1	2015-01-21 00:05:52	File-1.1.zip	f
\.


--
-- TOC entry 2308 (class 0 OID 0)
-- Dependencies: 202
-- Name: versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('versions_id_seq', 2, true);


--
-- TOC entry 2103 (class 2606 OID 16475)
-- Name: categories_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_primary PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 16477)
-- Name: currencies_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_primary PRIMARY KEY (id);


--
-- TOC entry 2109 (class 2606 OID 16479)
-- Name: orders_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_primary PRIMARY KEY (id);


--
-- TOC entry 2111 (class 2606 OID 16481)
-- Name: platforms_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY platforms
    ADD CONSTRAINT platforms_primary PRIMARY KEY (id);


--
-- TOC entry 2115 (class 2606 OID 16483)
-- Name: prices_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_primary PRIMARY KEY (id);


--
-- TOC entry 2119 (class 2606 OID 16485)
-- Name: products_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_primary PRIMARY KEY (id);


--
-- TOC entry 2121 (class 2606 OID 16487)
-- Name: publishers_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY publishers
    ADD CONSTRAINT publishers_primary PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 16489)
-- Name: purchases_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT purchases_primary PRIMARY KEY (id);


--
-- TOC entry 2128 (class 2606 OID 16491)
-- Name: types_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_primary PRIMARY KEY (id);


--
-- TOC entry 2130 (class 2606 OID 16493)
-- Name: users_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_primary PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 16495)
-- Name: versions_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT versions_primary PRIMARY KEY (id);


--
-- TOC entry 2104 (class 1259 OID 16496)
-- Name: fk_categories_types; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_categories_types ON categories USING btree ("typeId");


--
-- TOC entry 2107 (class 1259 OID 16497)
-- Name: fk_orders_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_orders_users ON orders USING btree ("userId");


--
-- TOC entry 2112 (class 1259 OID 16498)
-- Name: fk_prices_currency; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_prices_currency ON prices USING btree ("currencyId");


--
-- TOC entry 2113 (class 1259 OID 16499)
-- Name: fk_prices_product; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_prices_product ON prices USING btree ("productId");


--
-- TOC entry 2116 (class 1259 OID 16500)
-- Name: fk_products_categories; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_products_categories ON products USING btree ("categoryId");


--
-- TOC entry 2117 (class 1259 OID 16501)
-- Name: fk_products_publishers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_products_publishers ON products USING btree ("publisherId");


--
-- TOC entry 2122 (class 1259 OID 16502)
-- Name: fk_purchases_orders; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_purchases_orders ON purchases USING btree ("orderId");


--
-- TOC entry 2123 (class 1259 OID 16503)
-- Name: fk_purchases_prices; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_purchases_prices ON purchases USING btree ("priceId");


--
-- TOC entry 2124 (class 1259 OID 16504)
-- Name: fk_purchases_products; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_purchases_products ON purchases USING btree ("productId");


--
-- TOC entry 2131 (class 1259 OID 16505)
-- Name: fk_versions_products; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fk_versions_products ON versions USING btree ("productId");


--
-- TOC entry 2134 (class 2606 OID 16506)
-- Name: fk_categories_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_categories_types FOREIGN KEY ("typeId") REFERENCES types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2135 (class 2606 OID 16511)
-- Name: fk_orders_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT fk_orders_users FOREIGN KEY ("userId") REFERENCES users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2136 (class 2606 OID 16516)
-- Name: fk_prices_currencies; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fk_prices_currencies FOREIGN KEY ("currencyId") REFERENCES currencies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2137 (class 2606 OID 16521)
-- Name: fk_prices_products; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fk_prices_products FOREIGN KEY ("productId") REFERENCES products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2138 (class 2606 OID 16526)
-- Name: fk_products_categories; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT fk_products_categories FOREIGN KEY ("categoryId") REFERENCES categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2139 (class 2606 OID 16531)
-- Name: fk_products_publishers; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT fk_products_publishers FOREIGN KEY ("publisherId") REFERENCES publishers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2140 (class 2606 OID 16536)
-- Name: fk_purchases_orders; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT fk_purchases_orders FOREIGN KEY ("orderId") REFERENCES orders(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2141 (class 2606 OID 16541)
-- Name: fk_purchases_prices; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT fk_purchases_prices FOREIGN KEY ("priceId") REFERENCES prices(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2142 (class 2606 OID 16546)
-- Name: fk_purchases_products; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT fk_purchases_products FOREIGN KEY ("productId") REFERENCES products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2143 (class 2606 OID 16551)
-- Name: fk_versions_products; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT fk_versions_products FOREIGN KEY ("productId") REFERENCES products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-07-26 02:34:18 BST

--
-- PostgreSQL database dump complete
--

