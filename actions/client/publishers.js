import * as client from "../publishers";
import { getRecords } from 'riper/actions/client/repositories';

export const getByProductPublisherIds = client.getByProductPublisherIds(getRecords);