import * as client from '../categories';
import { getRecords } from 'riper/actions/client/repositories';
import { getByProductIds } from '../client/prices';

export const getByProductCategoryIds = client.getByProductCategoryIds(getRecords);
export const getBySlug = client.getBySlug(getRecords, getByProductIds);