import axios from "axios";
import * as types from "../types";
import { getByProductIds } from "./prices";

function getPrices() {
    return (dispatch, getState) => {
        const productIds = getState().basket.products.map(p => p.id);

        return dispatch(getByProductIds(productIds)).then(() => {
            dispatch(calculateTotal());
        })
    };
}

function saveProductIds() {
    return (dispatch, getState) => {
        const products = getState().basket.products;
        const user = getState().users.loggedInUser;

        if (user) {
            return axios.put('/users/' + user.id, {
                basketProductIds: products.map(product => product.id)
            });
        } else {
            return Promise.resolve();
        }
    };
}

function calculateTotal() {
    return {
        type: types.BASKET_CALCULATE_TOTAL
    };
}

export function addProduct(product) {
    return dispatch => {
        dispatch({
            type: types.BASKET_ADD_PRODUCT,
            product
        });

        dispatch(calculateTotal());

        return dispatch(saveProductIds());
    };
}

export function removeProduct(product) {
    return dispatch => {
        dispatch({
            type: types.BASKET_REMOVE_PRODUCT,
            product
        });

        dispatch(calculateTotal());

        return dispatch(saveProductIds());
    };
}

export function clear() {
    return dispatch => () => {
        dispatch({
            type: types.BASKET_CLEAR
        });

        return dispatch(saveProductIds());
    }
}

/**
 * Reload the products when component mounted to make sure that the products are still available and prices accurate
 */
export function reloadProducts() {
    return (dispatch, getState) => {
        const products = getState().basket.products;

        if (!products.length) {
            return Promise.resolve();
        }

        return axios.get('/products', {
            params: {
                id: products.map(product => product.id),
                active: true
            }
        }).then(response => {
            dispatch({
                type: types.BASKET_RELOAD_PRODUCTS,
                products: response.data
            });

            return Promise.all([
                dispatch(getPrices()),
                dispatch(saveProductIds())
            ]);
        });
    };
}

export function loadProductsFromUser() {
    return (dispatch, getState) => {
        const productIds = getState().basket.userProductIds;

        if (!productIds.length) {
            return Promise.resolve();
        }

        return axios.get('/products', {
            params: {
                id: productIds,
                active: true
            }
        }).then(response => {
            dispatch({
                type: types.BASKET_ADD_PRODUCTS,
                products: response.data
            });

            return Promise.all([
                dispatch(getPrices()),
                dispatch(saveProductIds())
            ]);
        });
    };
}
