import * as server from "../products";
import { getRecords, getRecord } from 'riper/actions/client/repositories';
import { getByProductIds } from '../client/prices';

export const getByCategory = server.getByCategory(getRecords, getByProductIds);
export const getBySlug = server.getBySlug(getRecords, getRecord, getByProductIds);
export const getFeatured = server.getFeatured(getRecords, getByProductIds);