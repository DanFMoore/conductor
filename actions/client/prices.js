import { getRecords } from 'riper/actions/client/repositories';

/**
 * @todo handle other currencies, with the currency id being pulled from getState possibly.
 *
 * @param {Array} ids
 * @returns {Function}
 */
export function getByProductIds(ids) {
    return getRecords('prices', {
        productId: ids
    });
}