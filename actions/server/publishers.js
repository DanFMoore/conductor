import * as client from "../publishers";
import getActions from 'riper/actions/server/repositories';
import * as repositories from "../../repositories";

const actions = getActions(repositories);

export const getByProductPublisherIds = client.getByProductPublisherIds(actions.getRecords);