import * as client from '../categories';
import getActions from 'riper/actions/server/repositories';
import * as repositories from "../../repositories";
import { getByProductIds } from '../server/prices';

const actions = getActions(repositories);

export const getByProductCategoryIds = client.getByProductCategoryIds(actions.getRecords);
export const getBySlug = client.getBySlug(actions.getRecords, getByProductIds);