import * as client from "../products";
import { getByProductIds } from '../server/prices';
import getActions from 'riper/actions/server/repositories';
import * as repositories from "../../repositories";

const actions = getActions(repositories);

export const getByCategory = client.getByCategory(actions.getRecords, getByProductIds);
export const getBySlug = client.getBySlug(actions.getRecords, actions.getRecord, getByProductIds);
export const getFeatured = client.getFeatured(actions.getRecords, getByProductIds);