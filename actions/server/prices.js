import bricks from 'sql-bricks-postgres';
import getActions from 'riper/actions/server/repositories';
import * as repositories from "../../repositories";

const actions = getActions(repositories);

/**
 * @param {Array} ids
 * @returns {Function}
 */
export function getByProductIds(ids) {
    return dispatch => {
        const now = new Date().toISOString();

        const params = [
            bricks.in('productId', ids),
            bricks.lte('start', now),
            bricks.or(bricks.gt('end', now), bricks.isNull('end'))
        ];

        return dispatch(actions.getRecords('prices', params));
    }
}