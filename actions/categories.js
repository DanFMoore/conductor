import { getByCategory } from "./products";
import { notFound } from "riper/actions/common";

export function getByProductCategoryIds(getRecords) {
    return () => (dispatch, getState) => {
        const ids = getState().products.categoryIds;

        return dispatch(getRecords('categories', {
            id: ids
        }));
    }
}

export function getBySlug(getRecords, getByProductIds) {
    return slug => (dispatch, getState) => {
        return dispatch(getRecords('categories', { slug })).then(() => {
            const category = getState().categories.records[0];

            if (category) {
                return dispatch(getByCategory(getRecords, getByProductIds)(category));
            } else {
                dispatch(notFound());
            }
        });
    }
}