import * as types from "./types";
import { getByProductPublisherIds } from "./publishers";
import { getByProductCategoryIds } from "./categories";
import { notFound } from "riper/actions/common";
import { notifyBasketProducts } from "./basket";

export function getByCategory(getRecords, getPricesByProductIds) {
    return category => (dispatch, getState) => {
        return dispatch(getRecords('products', {
            categoryId: category.id,
            active: true
        })).then(() => {
            dispatch({
                type: types.PRODUCT_ASSIGN_CATEGORY,
                category
            });

            const productIds = getState().products.records.map(product => product.id);

            return Promise.all([
                dispatch(getByProductPublisherIds(getRecords)()),
                dispatch(getPricesByProductIds(productIds))
            ]);
        });
    };
}

/**
 * @param {Function} getRecords
 * @param {Function} getRecord
 * @param {Function} getPricesByProductIds
 */
export function getBySlug(getRecords, getRecord, getPricesByProductIds) {
    return slug => (dispatch, getState) => {
        return dispatch(getRecords('products', {
            slug,
            active: true
        })).then(() => {
            const product = getState().products.records[0];

            if (product) {
                dispatch(notifyBasketProducts());

                return Promise.all([
                    dispatch(getRecord('categories', product.categoryId)),
                    dispatch(getRecord('publishers', product.publisherId)),
                    dispatch(getPricesByProductIds([product.id]))
                ]);
            } else {
                return dispatch(notFound());
            }
        });
    };
}

export function getFeatured(getRecords, getPricesByProductIds) {
    return () => (dispatch, getState) => {
        return dispatch(getRecords('products', {
            active: true,
            featured: true
        })).then(() => {
            const productIds = getState().products.records.map(product => product.id);

            return Promise.all([
                dispatch(getByProductCategoryIds(getRecords)()),
                dispatch(getByProductPublisherIds(getRecords)()),
                dispatch(getPricesByProductIds(productIds))
            ]);
        });
    };
}