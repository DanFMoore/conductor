export function getByProductPublisherIds(getRecords) {
    return () => (dispatch, getState) => {
        const ids = getState().products.publisherIds;

        return dispatch(getRecords('publishers', {
            id: ids
        }));
    }
}