import * as types from "./types";

export function notifyBasketProducts() {
    return (dispatch, getState) => {
        dispatch({
            type: types.BASKET_NOTIFY_PRODUCTS,
            products: getState().basket.products
        });
    };
}
