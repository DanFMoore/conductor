import Repository from 'riper/repository';
import connection from '../resources/connection';

export const categories = Repository('categories', ['id', 'name', 'typeId', 'description', 'slug'], connection);
export const currencies = Repository('currencies', ['id', 'name', 'code', 'symbol'], connection);
export const orders = Repository('orders', ['id', 'userId', 'date', 'complete'], connection);

export const prices = Repository(
    'prices',
    ['id', 'productId', 'currencyId', 'start', 'end', 'amount', 'previousAmount'],
    connection
);

export const products = Repository(
    'products',
    [
        'id',
        'name',
        'slug',
        'publisherId',
        'categoryId',
        'active',
        'shortDescription',
        'description',
        'platformIds'
    ],
    connection
);

export const publishers = Repository('publishers', ['id', 'name', 'email', 'phone'], connection);
export const purchases = Repository('purchases', ['id', 'orderId', 'priceId', 'productId'], connection);
export const types = Repository('types', ['id', 'name'], connection);

export const users = Repository('users',
    [
        'id',
        'username',
        'email',
        'name',
        'joinDate',
        'password',
        'active',
        'basketProductIds',
        'address1',
        'address2',
        'town',
        'county',
        'country',
        'postcode'
    ],
    connection
);

export const versions = Repository('versions', ['id', 'productId', 'versionNumber', 'date', 'filename'], connection);
