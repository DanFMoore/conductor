import { getByProductIds } from '../actions/server/prices';
import { useState, checkValidity } from 'riper/endpoints/helpers';
import { validate } from "riper/actions/server/request";

export default function(router) {
    router.get('/', (req, res, next) => {
        const store = res.locals.store;

        store.dispatch(validate(query => {
            if (!query.productId) {
                return {
                    productId: 'Required'
                };
            }
        }, null, req.query));

        if (!checkValidity(req, res)) {
            return;
        }

        store.dispatch(getByProductIds(req.query.productId)).then(() => {
            useState(res, state => {
                res.json(state.prices.records);
            });
        }).catch(next);
    });

    return router;
};